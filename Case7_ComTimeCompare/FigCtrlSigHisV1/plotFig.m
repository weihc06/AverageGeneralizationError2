clr_seq = [
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

close all;

%%
figure;
hold on;

line_width = 2;
line_clr = clr_seq(1,:);

for i=1:length(tc_his_all)
    subplot(length(tc_his_all),1,i);
    hold on
    tc_his = tc_his_all{i};
    ctrl_his = zeros(1,51);
    ctrl_his(tc_his+1) = 1;
%     scatter(tc_his,0*ones(1,length(tc_his)));
    plot([0 1],[0 0],'LineWidth',line_width,...
        'Color',line_clr);
    for k=2:length(ctrl_his)
        plot([k-1,k],[ctrl_his(k), ctrl_his(k)],...
            'LineWidth',line_width,'Color',line_clr);
        if ctrl_his(k-1)~=ctrl_his(k)
            plot([k-1,k-1],[0,1],...
                'LineWidth',line_width,'Color',line_clr);
        end
    end
    ylim([0 1]);
    xlim([0 50]);
    set(gca,'YTick',[0 1])
    set(gca,'FontSize',15);
    box on
    hold on
end

set(gcf,'Color','w');

hold off;

if 1
    saveas(gca,'./FigCtrlSigHis.fig');
end


%%
% for i=1:length(tc_his_all)
%     tc_his = tc_his_all{i};
%     scatter(tc_his,i*ones(1,length(tc_his)));
% end

%%
% for i=1:length(tc_his_all)
%     subplot(length(tc_his_all),1,i);
%     hold on
%     tc_his = tc_his_all{i};
% %     scatter(tc_his,0*ones(1,length(tc_his)));
%     for k=1:length(tc_his)
%         plot([tc_his(k),tc_his(k)],[0,1]);
%     end
%     ylim([0 1]);
%     set(gca,'YTick',[0 1])
%     hold off
% end