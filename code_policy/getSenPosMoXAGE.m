function [MoNext, x_new] = getSenPosMoXAGE(Mo, Cii, Cis, Css, sigma_n, Xs, varargin)
% Functionality:
%   This function generates the next sensing location index based on Mo
% Input
%   % Mo: nx_s by ny_s observation matrix/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
%   Cii: ni by ni covariance matrix of points of interests, 
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations, ns = nx_s*ny_s
%   sigma_n: standard variance of measurement noise
%   Xs: available sensing locations
% Output
%   MoNext: nxn matrix of next sensing location
%   2: should be measured at next time step
%   0: no measurement at next time step
%   x_new: new sensing location
% The calculation is simplified by using the property:
%   Cii(k+1) = Cii(k) - C(Xi,x)*C(x,Xi)/( C(x,x)+sigma_n^2 )
%            = Cii(k) - Cis(:,idx)*Cis(:,idx)'/( Css(idx,idx) + sigma_n^2 )
% Since Cii(k) is the same for every sensor, only Cis(:,idx)*Cis(:,idx)'/( Css(idx,idx) + sigma_n^2 )
% needs to be computated 
% In addition, since the error is tr(Cii(k+1)), only the trace of the
% subtraction needs to be computated
% Copyright (c) 2010-2016, Duke University

    
    
    if ~isempty(varargin) && varargin{2} == 1
        idx1 = varargin{1};
        MoNext = zeros(size(Mo));
        MoNext(idx1) = 2;
        x_new = Xs(:,idx1);
    else
        list_uncheck = find(Mo == 2); % find the matrix column-wise 
                                  % the list of sensing locations to be selected from
        nUncheck = length(list_uncheck); 
        age_reduce = zeros(1,nUncheck);
        
        % find the largest AGE reduction
        for i=1:nUncheck
            idx = list_uncheck(i); % index in Mo also in Css (idx_th column/row)
            v = Cis(:,idx);
            age_reduce(i) = v'*v / ( Css(idx,idx) + sigma_n^2 );
        end
        
        [~,i_opt] = max( age_reduce);
        idx_opt = list_uncheck(i_opt); % global index
        MoNext = zeros(size(Mo));
        MoNext(idx_opt) = 2;
    
        x_new = Xs(:,idx_opt);
    end
    

end