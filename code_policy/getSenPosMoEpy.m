function MoNext = getSenPosMoEpy(Mo, Css)
% Functionality:
%   This function generates the next sensing location index based on Mo and
%   by maximizing the entropy 
% Input
%   % Mo: nx_s by ny_s observation matrix/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
%   Css: ns by ns covariance matrix of sensing locations, ns = nx_s*ny_s
% Output
%   MoNext: nxn matrix of next sensing location
%   2: should be measured at next time step
%   0: no measurement at next time step
% Copyright (c) 2010-2016, Duke University


    list_uncheck = find(Mo == 2); % find the matrix column-wise
                                  % the list of sensing locations to be selected from
    v = diag(Css);
    v_uncheck = v(list_uncheck);
    
    [~,i_opt] = max( v_uncheck);
    idx_opt = list_uncheck(i_opt);
    
    MoNext = zeros(size(Mo));
    MoNext(idx_opt) = 2;

end