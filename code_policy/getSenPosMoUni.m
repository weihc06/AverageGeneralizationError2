function MoNext = getSenPosMoUni(Mo)
% Functionality:
%   This function generates the next sensing location index based on Mo
% Input
%   % Mo: nx_s by ny_s observation matrix/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
% Output
%   MoNext: nxn matrix of next sensing location
%   2: should be measured at next time step
%   0: no measurement at next time step
% Copyright (c) 2010-2016, Duke University 

    list_uncheck = find(Mo == 2);
    nUncheck = length(list_uncheck);

    % draw a random sensing location from the avaliable set
    rnd_list = unidrnd(nUncheck,1,1); % local index
    rnd_idx = list_uncheck( rnd_list ); % global index
    
    MoNext = zeros(size(Mo));
    MoNext(rnd_idx) = 2;

end