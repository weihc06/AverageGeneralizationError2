%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

clear
close all;

flag_save_fig = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

figure
hold on

%% Result 1
load('r1.mat');

%% theoretic result for Uniformly Random 
R1.tk = [];
R1.sn_err_his = [];
for k=1:length(sn.err_his)
    R1.tk = [R1.tk k];
    R1.sn_err_his = [R1.sn_err_his sn.err_his{k}(1)];
    if length(sn.err_his{k})>1
        R1.tk = [R1.tk k];
        R1.sn_err_his = [R1.sn_err_his sn.err_his{k}(2)];
    end
end

% individual sensors
% sensor_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor(i).err_his)
%         tk = [tk k];
%         sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(1)];
%         if length(sensor(i).err_his{k})>1
%             tk = [tk k];
%             sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_err_his{i},'LineWidth',2,'Color',clr_seq(1,:));
% end

R1.tk = [0 R1.tk];
R1.sn_err_his = [1 R1.sn_err_his];

%% simulation result for Uniformly Random 
% tk = [];
% sn_uni_err_his = [];
% for k=1:length(sn_uni.err_his)
%     tk = [tk k];
%     sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(1)];
%     if length(sn_uni.err_his{k})>1
%         tk = [tk k];
%         sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(2)];
%     end
% end
% h(2) = plot(tk, sn_uni_err_his,'LineWidth',2,'Color',clr_seq(2,:));
% individual sensors
% sensor_uni_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor_uni(i).err_his)
%         tk = [tk k];
%         sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(1)];
%         if length(sensor_uni(i).err_his{k})>1
%             tk = [tk k];
%             sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_uni_err_his{i},'LineWidth',2,'Color',clr_seq(2,:));
% end

%% simulation result for Greedy Algorithm that maximizes AGE
R1.sn_age_err_his = [];
for k=1:length(sn_age.err_his)
    R1.sn_age_err_his = [R1.sn_age_err_his sn_age.err_his{k}(1)];
    if length(sn_age.err_his{k})>1
        R1.sn_age_err_his = [R1.sn_age_err_his sn_age.err_his{k}(2)];
    end
end
R1.sn_age_err_his = [1 R1.sn_age_err_his];

% individual sensors
R1.tcom = []; 
R1.sensor_age_err_his = cell(1,sPar.ns);
for i=1%:sPar.ns
    for k=1:length(sensor_age(i).err_his)
        R1.sensor_age_err_his{i} = [R1.sensor_age_err_his{i} sensor_age(i).err_his{k}(1)];
        if length(sensor_age(i).err_his{k})>1
            R1.tcom(end+1) = k;
            R1.sensor_age_err_his{i} = [R1.sensor_age_err_his{i} sensor_age(i).err_his{k}(2)];
        end
    end
end

R1.sensor_age_err_his{1} = [1 R1.sensor_age_err_his{1}];
%% simulation result for Greedy Algorithm that maximizes Entropy
R1.sn_epy_err_his = [];
for k=1:length(sn_epy.err_his)
    R1.sn_epy_err_his = [R1.sn_epy_err_his sn_epy.err_his{k}(1)];
    if length(sn_epy.err_his{k})>1
        R1.sn_epy_err_his = [R1.sn_epy_err_his sn_epy.err_his{k}(2)];
    end
end
% individual sensors
R1.sensor_epy_err_his = cell(1,sPar.ns);
for i=1%:sPar.ns
    for k=1:length(sensor_epy(i).err_his)
        R1.sensor_epy_err_his{i} = [R1.sensor_epy_err_his{i} sensor_epy(i).err_his{k}(1)];
        if length(sensor_epy(i).err_his{k})>1
            R1.sensor_epy_err_his{i} = [R1.sensor_epy_err_his{i} sensor_epy(i).err_his{k}(2)];
        end
    end
end
R1.sensor_epy_err_his{i} = [1 R1.sensor_epy_err_his{i}];

%% Result 2
R2 = load('r2.mat');

%% simulation result for Greedy Algorithm that maximizes AGE
% network
% figure; hold on;
R2.tk = [];
R2.sn_age_err_his = [];
for k=1:length(R2.sn_age.err_his)
    R2.tk = [R2.tk k];
    R2.sn_age_err_his = [R2.sn_age_err_his R2.sn_age.err_his{k}(1)];
    if length(R2.sn_age.err_his{k})>1
        R2.tk = [R2.tk k];
        R2.sn_age_err_his = [R2.sn_age_err_his, R2.sn_age.err_his{k}(2)];
    end
end
R2.tk = [0 R2.tk];
R2.sn_age_err_his = [1 R2.sn_age_err_his];

% individual sensors
R2.tcom = []; 
R2.sensor_age_err_his = cell(1,R2.sPar.ns);
for i=1%:sPar.ns
    for k=1:length(R2.sensor_age(i).err_his)
        R2.sensor_age_err_his{i} = [R2.sensor_age_err_his{i}, R2.sensor_age(i).err_his{k}(1)];
        if length(R2.sensor_age(i).err_his{k})>1
            R2.tcom(end+1) = k;
            R2.sensor_age_err_his{i} = [R2.sensor_age_err_his{i}, R2.sensor_age(i).err_his{k}(2)];
        end
    end
    R2.sensor_age_err_his{i} = [1 R2.sensor_age_err_his{i}];
end

%% plot FigComByGdyR1And2
h1 = plot(R1.tk, R1.sn_err_his,'LineWidth',2,'Color','k','LineStyle','--');
h3 = plot(R1.tk, R1.sn_age_err_his, 'LineStyle','-.','LineWidth',2,'Color',rgb('ForestGreen'));
h31 = plot(R1.tk, R1.sensor_age_err_his{1},'LineWidth',2,'Color','r');

hc = scatter(tcom, ones(1,length(tcom))*0.005,30,'k','o');
for t = tcom
    plot([t,t],[0 1],':','Color','k');
end
box on

font_size = 15;

legend([h1,h31, h3, hc],{'$\hat{\epsilon}_u(k)$','$\epsilon_g[Q_1(k)]$',...
    '$\epsilon_g[Q(k)]$','communication time'},'Interpreter','latex','FontSize',font_size)

xlabel('time step, $k$','Interpreter','latex','FontSize',font_size);
ylabel('$\epsilon(k)$', 'Interpreter','latex','FontSize',font_size);
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',font_size,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off

if 0
    saveas(gcf,'./FigComByGdyR1and2.fig');
    export_fig ./FigComByGdyR1And2.png;
    export_fig ./FigComByGdyR1And2.pdf;
end

%% plot FigGdyVsEpySingle
figure
hold on

h31 = plot(R1.tk, R1.sensor_age_err_his{1},'LineWidth',2,'Color','r');
h41 = plot(R1.tk, R1.sensor_epy_err_his{i},'LineWidth',2,'Color',clr_seq(1,:),'LineStyle','--');

hc = scatter(tcom, ones(1,length(tcom))*0.005,30,'k','o');
for t = tcom
    plot([t,t],[0 1],':','Color','k');
end
box on

font_size = 15;

legend([h31, h41, hc],{'$\epsilon_g[Q_1(k)]$',...
    '$\epsilon_e[Q_1(k)]$','communication time'},'Interpreter','latex','FontSize',font_size)

xlabel('time step, $k$','Interpreter','latex','FontSize',font_size);
ylabel('$\epsilon(k)$', 'Interpreter','latex','FontSize',font_size);
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',font_size,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off

if 0
    saveas(gcf,'./FigGdyVsEpySingle.fig');
    export_fig ./FigGdyVsEpySingle.png;
    export_fig ./FigGdyVsEpySingle.pdf;
end

%% plot FigGdyVsGdyHyperSingle
figure
hold on

h31 = plot(R1.tk, R1.sensor_age_err_his{1},'LineWidth',2,'Color','r');
h31R2 = plot(R2.tk, R2.sensor_age_err_his{1},'LineWidth',2,'Color',rgb('Purple'),'LineStyle','--');

hc = scatter(tcom, ones(1,length(tcom))*0.005,30,'k','o');
for t = tcom
    plot([t,t],[0 1],':','Color','k');
end
box on

font_size = 15;

legend([h31, h31R2, hc],{'$\epsilon_g(Q_1,k)$',...
    '$\epsilon_g^*(Q_1,k)$','communication time'},'Interpreter','latex','FontSize',font_size)

xlabel('time step, $k$','Interpreter','latex','FontSize',font_size);
ylabel('$\epsilon(k)$', 'Interpreter','latex','FontSize',font_size);
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',font_size,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off

if 0
    saveas(gcf,'./FigGdyVsGdyHyperSingle.fig');
    export_fig ./FigGdyVsGdyHyperSingle.png;
    export_fig ./FigGdyVsGdyHyperSingle.pdf;
end

