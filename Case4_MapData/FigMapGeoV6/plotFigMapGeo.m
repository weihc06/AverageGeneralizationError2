%% add necessary libraries
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

flag_save_fig = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Red'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');


%% plot regression result without hyper-parameter optimization

% process all measurement inputs and outputs
X_new_plot = [];
y_new_plot = [];

sensor_plot = sensor_age;   
sn_plot = sn_age;

for i=1:length(sensor_plot)
    X_new_plot = [X_new_plot, sensor_plot(i).X_new];
    y_new_plot = [y_new_plot, sensor_plot(i).y_new];
end

Xa_plot = [sn_plot.Xa, X_new_plot];
ya_plot = [sn_plot.ya, y_new_plot];
    

ng_plot = [140, 60]*2;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot, fs2_plot] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);
fmu_plot( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot') == 0) = NaN;




%% plot workspace, and measurement sequence
figure

hold on

% surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'LineStyle','none','FaceColor','interp');
% scatter3(ws.Xs(1,:), ws.Xs(2,:), ones(1,size(ws.Xs,2))*100, 200, rgb('Red'),'.');
[~, fmu_plot2] = getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot', sPar.sigma_n);

% plot the figure

% surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')

R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);


    
Z = reshape(fmu_plot2, ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);

% show the states boundary
states = shaperead('usastatehi', 'UseGeoCoords', true);
hg1 = geoshow(hg,states,'DefaultFaceColor','none','DefaultEdgeColor',rgb('Black'),'DefaultLineWidth',2);

hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(13).LineStyle = ':';
hg.Children(13).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end



% plot sensing locations
for i=1:sPar.ns
%     scatter3(ws.Xs(1,sensor_age(i).Mo==2), ws.Xs(2,sensor_age(i).Mo==2), ones(1,sum(sum(sensor_age(i).Mo==2)))*100, 200, clr_seq_ws(i,:),'.');
%     p1 = geopoint(ws.Xs(2,sensor_age(i).Mo==2), ws.Xs(1,sensor_age(i).Mo==2));
    p1 = geopoint(ws.Xs(2,sensor_age(i).Mo==2), ws.Xs(1,sensor_age(i).Mo==2));
    geoshow(p1.Latitude, p1.Longitude,'LineStyle','none','Marker','.','Color',clr_seq_ws(i,:),'MarkerSize',15);
end

% plot points of interests
    p = geopoint( ws.Xi(2,:), ws.Xi(1,:) ); % p = geopoint(y,x);
    hgi = geoshow(p.Latitude, p.Longitude, 'LineStyle','none','Marker','d',...
        'Color',rgb('Black'),'MarkerSize',4, 'MarkerFaceColor',rgb('Black'));
    


% figure properties
set(gcf,'Color','w');
% legend({ 'Ground-truth Fun', 'Sensing Locations'},'Location','northeastoutside'); 
% % % hl = legend('a','b',' c','d','e','f','g');
% % % hl.FontSize=15;
hc = colorbar;
% hc.Label.String = 'Function Value';
% set(hc.Label,'FontName','Timesnewroman','Interpreter','latex')
% hc.Label.FontSize = 15;
% hc.Label.Rotation = 0;
% hc.Label.Position = hc.Label.Position + [3 0 0 ];
% hc.Label.Rotation = -90;
hc.Units = 'pixels';
% hc.Position(4) = hc.Position(4)*0.6;
% hc.Position(1) = hc.Position(1) + 20;
hc.FontSize = 15;
hc.Position = [484 118.384322870868 19.9999996182028 207.615677129132];
if 0
    saveas(gcf,'./FigMapGeo.fig');
    export_fig './FigMapGeo.png' -r500;
%     export_fig './FigMapGeo.pdf';
end
hold off







