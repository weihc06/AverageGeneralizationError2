% This scripture sets up the configurations of the test case
% Author: Hongchuan Wei

% Copyright (c) 2010-2016, Duke University 

%% add necessary libraries
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

rng(23);

%% Workspace/Environment parameters

%%%%%%%%%%%%      Parameters of geometry of workspace             %%%%%%%%%
ws.type = 'map'; % type of workspace

%%%%%%%%%%%%%     read data  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path_name = '..\..\code_training_data\PRISM_tmean_provisional_4kmM2_201603_bil\';
file_name = 'PRISM_tmean_provisional_4kmM2_201603_bil';

% read header file
path_file_name_hdr = [path_name, file_name, '.hdr'];
data_file_hdr = fopen(path_file_name_hdr);
header = textscan(data_file_hdr,'%s',28);
data_hdr.BYTEORDER = header{1}{2};
data_hdr.LAYOUT = header{1}{4};
data_hdr.NROWS  = str2num(header{1}{6});
data_hdr.NCOLS  = str2num(header{1}{8});
data_hdr.NBANDS = str2num(header{1}{10});
data_hdr.NBITS  = header{1}{12};
data_hdr.PIXELTYPE = lower(header{1}{18});
data_hdr.ULXMAP = str2num(header{1}{20});
data_hdr.ULYMAP = str2num(header{1}{22});
data_hdr.XDIM   = str2num(header{1}{24});
data_hdr.YDIM   = str2num(header{1}{26});
data_hdr.NODATA = str2num(header{1}{28});
% compose parameters for multibandread function
data_hdr.offset = 0; % since the header is not included in this file
data_hdr.size = [ data_hdr.NROWS, data_hdr.NCOLS, data_hdr.NBANDS]; % [ NROWS NCOLS NBANDS]
data_hdr.interleave = data_hdr.LAYOUT;
data_hdr.precision = [data_hdr.PIXELTYPE, data_hdr.NBITS];
if strcmp(data_hdr.BYTEORDER, 'I')
    data_hdr.byteorder = 'ieee-le';              % BYTEORDER = I (refers to Intel order or little endian format)
else
    error('data_hdr.BYTEORDER type error');
end

%%%%%%%%%%%%%     read data from data file  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path_file_name_bil = [path_name, file_name, '.bil'];
% temperature unit: Celsius degree
data_file_bil = multibandread(path_file_name_bil, ...
    data_hdr.size, data_hdr.precision, data_hdr.offset, data_hdr.interleave, data_hdr.byteorder);

data_file_bil_plot = flipud(data_file_bil);
data_file_bil_plot( data_file_bil_plot == data_hdr.NODATA ) = NaN;
% NOTICE that dim1 of data_file_bil_plot is latitude data (Y direction)
%             dim2 of data_file_bil_plot is longitude data (X direction)

%%%%%%%%%%%%% processing data for workspace   %%%%%%%%%%%%%
ws.data_file_bil_plot = data_file_bil_plot;
% clearvars data_file_bil_plot data_file_bil
data_hdr.ULYMAP2 = data_hdr.ULYMAP - data_hdr.YDIM * (data_hdr.NROWS-1);
ws.x0 = [data_hdr.ULXMAP, data_hdr.ULYMAP2]; % origin of workspace
ws.Lx = (data_hdr.NCOLS-1)*data_hdr.XDIM; % X, Y length of workspace
ws.Ly = data_hdr.YDIM * (data_hdr.NROWS-1);
ws.dx = data_hdr.XDIM;
ws.dy = data_hdr.YDIM;
ws.x_mesh = ws.x0(1): ws.dx: ws.x0(1)+ws.Lx;
ws.y_mesh = ws.x0(2): ws.dy: ws.x0(2)+ws.Ly;

%%%%%%%%%%%%      Parameters of accessible sensing locations      %%%%%%%%%
ws.sensing_dis = 'random'; % distribution of sensing locations
ws.n_s = 80; % the number of accessible sensing locations in total
ii = 1;
ws.sx_s = zeros(1,ws.n_s);
ws.sy_s = zeros(1,ws.n_s);
ws.Xs = zeros(2,ws.n_s);
while ii <= ws.n_s
    xt = [rand*ws.Lx; rand*ws.Ly] + ws.x0';
    if getMeasurementFromRealData(ws.data_file_bil_plot', ws, xt) % 1 is data, 0 not data
        ws.Xs(:,ii) = xt;
        ii = ii+1;
    end
end
ws.sx_s = ws.Xs(1,:); ws.sy_s = ws.Xs(2,:);

% sort the sensing locations
ws.x1 = [ws.x0(1), ws.x0(2) + ws.Ly*0.66];
ws.x2 = [ws.x0(1) + ws.Lx, ws.x0(2) + ws.Ly*0.45];
ws.x3 = [ws.x0(1) + ws.Lx*0.45, ws.x0(2)];
ws.x4 = [ws.x0(1) + ws.Lx*0.45, ws.x0(2)+ws.Ly];

ws.line1 = @(x) (ws.x1(2)-ws.x2(2))/(ws.x1(1)-ws.x2(1))*(x-ws.x1(1)) + ws.x1(2);

Xs12 = ws.Xs(:, ws.Xs(1,:) <= ws.x3(1) );
Xs34 = ws.Xs(:, ws.Xs(1,:) > ws.x3(1) );
Xs1 = Xs12(:, Xs12(2,:) <= ws.line1( Xs12(1,:) ) );
Xs2 = Xs12(:, Xs12(2,:) >  ws.line1( Xs12(1,:) ) );
Xs3 = Xs34(:, Xs34(2,:) <= ws.line1( Xs34(1,:) ) );
Xs4 = Xs34(:, Xs34(2,:) >  ws.line1( Xs34(1,:) ) );

ws.Xs = [Xs1, Xs2, Xs3, Xs4];
ws.n_s_vec = [size(Xs1,2), size(Xs2,2), size(Xs3,2), size(Xs4,2)];

% figure
% hold on
% scatter(ws.Xs(1,:), ws.Xs(2,:),'.');
% scatter(Xs1(1,:), Xs1(2,:),'o');
% scatter(Xs2(1,:), Xs2(2,:),'o');
% scatter(Xs3(1,:), Xs3(2,:),'o');
% scatter(Xs4(1,:), Xs4(2,:),'o');

%%%%%%%%%%%%      Parameters of test points/points of interest   %%%%%%%%%%
ws.poi_dis = 'grid'; % distribution of points of interest
% sx_i = ws.x0(1)+0.1: ws.dx*100: ws.x0(1)+ws.Lx;
% sy_i = ws.x0(2)+0.1: ws.dy*100: ws.x0(2)+ws.Ly;
sx_i = ws.x0(1)+0.1: ws.dx*150: ws.x0(1)+ws.Lx;
sy_i = ws.x0(2)+0.1: ws.dy*110: ws.x0(2)+ws.Ly;
[X_mesh, Y_mesh] = meshgrid( sx_i, sy_i);
X_mesh_v = X_mesh(:)';
Y_mesh_v = Y_mesh(:)';
tmp_if_data = getMeasurementFromRealData(ws.data_file_bil_plot', ws, [X_mesh_v; Y_mesh_v]);
idx_if_data = find(tmp_if_data);
ws.sx_i = X_mesh_v(idx_if_data);
ws.sy_i = Y_mesh_v(idx_if_data);
ws.Xi = [ws.sx_i; ws.sy_i]; % 2xn points of interest, for the laziness

%%%%%%%%%%%%      parameters of simulation time                %%%%%%%%%%%%
ws.tf = 50; % simulation total time, (ws.dt = 1, implied)

sPar.ns = 4; % number of sensors

%% plot map using geo
flag_plot_map_geo = 0
if flag_plot_map_geo
    figure
    hold on
    R = georasterref('RasterSize',size(ws.data_file_bil_plot),...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
    Z = ws.data_file_bil_plot;
    
    hg = worldmap(Z,R);

    hgf = geoshow(Z,R,'DisplayType','surface',...
        'ZData',zeros(size(Z)),'CData',Z);


    hg.Children(12).LineStyle = ':';
    hg.Children(12).LineWidth = 0.5;
%     for i=1:8
%         hg.Children(i).FontSize = 14;
%     end
    
    % show the states map
%     states = shaperead('usastatehi', 'UseGeoCoords', true);
%     hg1 = geoshow(states,'DefaultFaceColor','none','DefaultEdgeColor','w','LineStyle',':');

    % show the subworkspace
    
    p1 = geopoint([ws.x3(2), ws.x4(2)], [ws.x3(1), ws.x4(1)]);
    geoshow(p1.Latitude, p1.Longitude,'LineStyle','--','Color','w','LineWidth',1);
    xplot = linspace( ws.x1(1), ws.x2(1), 100);
%     xplot = [ ws.x1(1), ws.x3(1), ws.x2(1)];
    yplot = (ws.x2(2) - ws.x1(2))/(ws.x2(1) - ws.x1(1))*(xplot - ws.x1(1)) + ws.x1(2);
    p2 = geopoint(yplot,xplot);
    geoshow(p2.Latitude, p2.Longitude,'LineStyle','--','Color','w','LineWidth',1);
    
    % try showing a line
    % y = [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP];
    % x = [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM];
    % p = geopoint(y,x);
    % geoshow(p.Latitude, p.Longitude);
    
    % plot sensing locations
    p = geopoint( ws.Xs(2,:), ws.Xs(1,:) ); % p = geopoint(y,x);
    hgs = geoshow(p.Latitude, p.Longitude, 'LineStyle','none','Marker','.','Color','r','MarkerSize',15);
    
    % plot points of interests
    p = geopoint( ws.Xi(2,:), ws.Xi(1,:) ); % p = geopoint(y,x);
    hgi = geoshow(p.Latitude, p.Longitude, 'LineStyle','none','Marker','d',...
        'Color',rgb('Black'),'MarkerSize',4, 'MarkerFaceColor',rgb('Black'));
    
    % annotate workspace, textm(lat,lon,string)
    textm(30,-120,'$\mathcal{W}_1$','Interpreter','latex','FontSize',15)
    textm(45,-130,'$\mathcal{W}_2$','Interpreter','latex','FontSize',15)
    textm(30,-77,'$\mathcal{W}_3$','Interpreter','latex','FontSize',15)
    textm(48,-77,'$\mathcal{W}_4$','Interpreter','latex','FontSize',15)
    
    
    % figure properties
%     legend([hgf, hgs, hgi],{'temperature','sensing locations','points of interest'},'Interpreter','latex','FontSize',15);
    set(gca,'FontSize',15);
    set(gcf,'Color','w');
    if 0
        export_fig ./FigMapGeoV3.png -r500
        export_fig ./FigMapGeoV3.pdf -r500
        
        %     savefig(gcf,'FigMapGeo.fig');
    end
    hold off
end

%% plot map using surf
flag_plot_map_surf = 0
if flag_plot_map_surf
    figure
    hold on
    surf(ws.x_mesh, ws.y_mesh, ws.data_file_bil_plot,'LineStyle','none');    
    view(2)
    
    % plot sensing locations
    scatter3(ws.Xs(1,:), ws.Xs(2,:), 100*ones(1,size( ws.Xs,2) ), 200, rgb('Red'),'.')
    
    % plot points of interests
    scatter3(ws.Xi(1,:), ws.Xi(2,:), 100*ones(1,size( ws.Xi,2) ), 25, rgb('Gold'),'d','filled')
    
    legend({'Spatial Phenomenon','Sensing location','Points of interest'},'Location','southoutside')
    colorbar
    axis equal
    axis tight
    grid on
    if 0
        export_fig ./FigMapSurf.png -r300
%         export_fig ./FigMapSurf.pdf -r300
%         savefig(gcf,'FigMapSurf.fig');
    end
    hold off
end


%% Gaussian Process Parameters for Latent Variables/Function/GP process

% latent function related 
GP_Par.covfunc = @covSEiso; % squared exponential cov
GP_Par.hyp.cov = [log(sqrt(100)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T

% measurement/observation/training data related
GP_Par.likfunc = @likGauss; % Gaussian likelihood function/measurement func
GP_Par.sigma_n = sqrt(0.1); % standard deviation of measurement noise
GP_Par.hyp.lik = log(GP_Par.sigma_n); % log(sigma_n)


%% Sensor paparameters

sPar.comTh = 0.4; % communication threshold
sPar.sigma_n = GP_Par.sigma_n; % measurement noise standard variance

%% Sensor Network Structs for Theoretical Curves, Uniform Random

%%%%%%%%%%%%      Parameters related to measurements/training data %%%%%%%%
%%%%%%%%%%%%      of the entire sensor network                     %%%%%%%%

% Mo: initial observation vector/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
sn.Mo = 2*ones(1, ws.n_s ); % assume that all positions can be measured

%..........................................................................
% ....    test if the algorithm works when there are measurement     .... %
% ....    that are already obtained at the initial time              .... %
% nO = 0; % floor(ws.ns/3); % running test of the algorithm
% random_tmp = unidrnd( ws.nx_s, 2, nO);
% for jj = 1:size(random_tmp,2)
%     sn.Mo(random_tmp(1,jj), random_tmp(2,jj)) = 1;
% end
%..........................................................................

% matrix of observed measurement positions, d x n matrix, where d is the
% dimension of input, n is the number of observations
sn.Xa0 = [ws.sx_s(find(sn.Mo == 1));...
          ws.sy_s(find(sn.Mo == 1))]; 
sn.Xa = sn.Xa0;
% get noisy measurements from an underlying GP process determined by
% GP_Par0
% ws.ya is a vector of 1 x n matrix/vector, containing all the noisy
% measurements
sn.ya0 = getMeasurementFromRealData(ws.data_file_bil_plot', ws, sn.Xa0, sPar.sigma_n);
sn.ya = sn.ya0;


%%%%%%%%%%%%      get initial covariance matrices              %%%%%%%%%%%%
% [varargout] = gp(hyp, inf, mean, cov, lik, x, y, xs, ys)
if ~isempty(sn.Xa0) 
    [~, ~, ~, fs2g] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, sn.Xa0', sn.ya0, ws.Xi');
    sn.Cii0 = fs2g;
    [~, ~, ~, fs2g] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, sn.Xa0', sn.ya0, ws.Xs');
    sn.Css0 = fs2g;
    
    Caa = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa0', sn.Xa0') + eye(size(sn.Xa0,2))*(GP_Par.sigma_n^2);
    Cis = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', ws.Xs');
    Cia = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', sn.Xa0');
    Cas = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa0', ws.Xs');    
    sn.Cis0 = Cis - Cia /Caa *Cas;
else
    % initial covariance matrices
    sn.Cii0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi'); % x: n by D matrix of inputs
    sn.Cis0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', ws.Xs'); % x: n by D matrix of inputs
    sn.Css0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xs');
end
% matrices without '0' are subject to change
sn.Cii = sn.Cii0;
sn.Cis = sn.Cis0;
sn.Css = sn.Css0;

sn.err_his = {}; % history of generalization errors
                 % assumed to be cell array since two values can occur at
                 % the time of cummunication
sn.tc = 0; % latest communication time


%% Other Sensor Network Structs for Experimental Results 
sn_uni = sn;         % simulation result for Uniformly Random 
sn_age = sn;         % simulation result for Greedy Algorithm that maximizes AGE
sn_epy = sn;         % simulation result for Greedy Algorithm that maximizes Entropy


%% Individual Sensors for Theoretical Results, Uniform Random Algorithm
for i=1:sPar.ns
    sensor(i) = sn;
end
for i=1:sPar.ns % additional features in subworkspace
    %   Mo: matrix stores which positions have been observed
    %       0: observed, but could not obtain measurement
    %       1: observed, and obtained measurement
    %       2: positions not checked yet
    sensor(i).Mo = zeros(1,ws.n_s); % mask of sensing locations
    sensor(i).Xa0 = [];
    sensor(i).ya0 = [];
    sensor(i).Xa = sensor(i).Xa0; % 2xn matrix of inputs before last communication, all sensors'
    sensor(i).ya = sensor(i).ya0; % vector of outputs before last communication 
    sensor(i).Xl = sensor(i).Xa0; % 2xn matrix of inputs, local sensor's
    sensor(i).yl = sensor(i).ya0; 
    sensor(i).err_his = {}; % history of generalization errors
    sensor(i).X_new = [];  % vector of new observed positions/input 
    sensor(i).y_new = [];   % row vector of new measurements/observations/output data
end
% setup masks of sensing locations for every sensor
sensor(1).Mo(1:ws.n_s_vec(1)) = 2;
sensor(2).Mo(ws.n_s_vec(1)+1: sum(ws.n_s_vec(1:2)) ) = 2;
sensor(3).Mo(sum(ws.n_s_vec(1:2))+1: sum(ws.n_s_vec(1:3)) ) = 2;
sensor(4).Mo(sum(ws.n_s_vec(1:3))+1: sum(ws.n_s_vec(1:4)) ) = 2;
% the index of the first measurement in Mo, in order to make the initial
% measurement position the same
sensor(1).idx1 = 12; sensor(2).idx1 = 38; sensor(3).idx1 = 62; sensor(4).idx1 = 87;


%% Individual Sensors for Simulation Results
sensor_uni = sensor; % simulation result for Uniformly Random 
sensor_age = sensor; % simulation result for Greedy Algorithm that maximizes AGE
sensor_epy = sensor; % simulation result for Greedy Algorithm that maximizes Entropy





