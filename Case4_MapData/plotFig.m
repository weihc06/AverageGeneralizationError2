%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

flag_save_fig = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

figure
hold on
tcom = [];
%% theoretic result for Uniformly Random 
tk = [];
sn_err_his = [];
for k=1:length(sn.err_his)
    tk = [tk k];
    
    sn_err_his = [sn_err_his sn.err_his{k}(1)];
    if length(sn.err_his{k})>1
        tk = [tk k];
        tcom(end+1) = k;
        sn_err_his = [sn_err_his sn.err_his{k}(2)];
    end
end
h1 = plot(tk, sn_err_his,'LineWidth',2,'Color','k','LineStyle','--');
% individual sensors
% sensor_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor(i).err_his)
%         tk = [tk k];
%         sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(1)];
%         if length(sensor(i).err_his{k})>1
%             tk = [tk k];
%             sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_err_his{i},'LineWidth',2,'Color',clr_seq(1,:));
% end

%% simulation result for Uniformly Random 
% tk = [];
% sn_uni_err_his = [];
% for k=1:length(sn_uni.err_his)
%     tk = [tk k];
%     sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(1)];
%     if length(sn_uni.err_his{k})>1
%         tk = [tk k];
%         sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(2)];
%     end
% end
% h(2) = plot(tk, sn_uni_err_his,'LineWidth',2,'Color',clr_seq(2,:));
% % individual sensors
% sensor_uni_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor_uni(i).err_his)
%         tk = [tk k];
%         sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(1)];
%         if length(sensor_uni(i).err_his{k})>1
%             tk = [tk k];
%             sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_uni_err_his{i},'LineWidth',2,'Color',clr_seq(2,:));
% end

%% simulation result for Greedy Algorithm that maximizes AGE
% network
% figure; hold on;
tk = [];
sn_age_err_his = [];
for k=1:length(sn_age.err_his)
    tk = [tk k];
    sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(1)];
    if length(sn_age.err_his{k})>1
        tk = [tk k];
        sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(2)];
    end
end
h3 = plot(tk, sn_age_err_his,'LineStyle','-.','LineWidth',2,'Color',clr_seq(1,:));

% individual sensors
sensor_age_err_his = cell(1,sPar.ns);
for i=1%:sPar.ns
    tk = [];
    for k=1:length(sensor_age(i).err_his)
        tk = [tk k];
        sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(1)];
        if length(sensor_age(i).err_his{k})>1
            tk = [tk k];
            sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(2)];
        end
    end
    h31 = plot(tk, sensor_age_err_his{i},'LineWidth',2,'Color','r');
end


%% simulation result for Greedy Algorithm that maximizes Entropy

tk = [];
sn_epy_err_his = [];
for k=1:length(sn_epy.err_his)
    tk = [tk k];
    sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(1)];
    if length(sn_epy.err_his{k})>1
        tk = [tk k];
        sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(2)];
    end
end
h4 = plot(tk, sn_epy_err_his,'LineWidth',2,'Color',FORESTGREEN,'LineStyle',':');
% % individual sensors
% sensor_epy_err_his = cell(1,sPar.ns);
% for i=1%:sPar.ns
%     tk = [];
%     for k=1:length(sensor_epy(i).err_his)
%         tk = [tk k];
%         sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(1)];
%         if length(sensor_epy(i).err_his{k})>1
%             tk = [tk k];
%             sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_epy_err_his{i},'LineWidth',2,'Color',clr_seq(4,:));
% end


%%
hc = scatter(tcom, ones(1,length(tcom))*0.005,30,clr_seq(1,:),'o');
for t = tcom
    plot([t,t],[0.005 1],'-.','Color',clr_seq(1,:));
%     semilogy([t,t],[0 1],'-.','Color',clr_seq(1,:));
end

%% labels
box on
legend([h1,h31, h3, h4, hc],{'$\hat{\epsilon}_u(k)$','$\epsilon_g(Q_1,k)$',...
    '$\epsilon_g(Q,k)$','$\epsilon_e(Q,k)$','communication time'},'Interpreter','latex')

set(gca,'YScale','log');
axis tight

xlabel('time step, $k$','Interpreter','latex');
ylabel('average generalization error, $\epsilon$', 'Interpreter','latex');
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off

if 0
    export_fig ./FigAGECom.png -r300;
    export_fig ./FigAGECom.pdf -r500
end





%% plot regression result without hyper-parameter optimization

% process all measurement inputs and outputs
X_new_plot = [];
y_new_plot = [];

sensor_plot = sensor_age;   
sn_plot = sn_age;

for i=1:length(sensor_plot)
    X_new_plot = [X_new_plot, sensor_plot(i).X_new];
    y_new_plot = [y_new_plot, sensor_plot(i).y_new];
end

Xa_plot = [sn_plot.Xa, X_new_plot];
ya_plot = [sn_plot.ya, y_new_plot];
    

ng_plot = [140, 60]*2;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot, fs2_plot] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);
fmu_plot( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot') == 0) = NaN;

% plot the figure
figure
hold on
% 1. just plain surf
% surf(X1_plot, X2_plot, reshape(fmu_plot, ng_plot(2), ng_plot(1)), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function

% 2. geo
R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(fmu_plot, ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
% axesm eckert4; % might not be a good idea
% R1 = [1/ws.dx, ws.x0(2)+ws.Ly, ws.x0(1)];
% hgf = geoshow(reshape(fmu_plot, ng_plot(2), ng_plot(1)),R1,'DisplayType','surface');

set(gcf,'Color','w');

if flag_save_fig
    export_fig ./FigPredictMeanFcn.png -r500;
    export_fig ./FigPredictMeanFcn.pdf -r500
    
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcn','-dpdf')
end
hold off

%% optimize hyper-parameter

hyp_opt = minimize(GP_Par.hyp, @gp, -100, @infExact, [], ...
    GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot'); % optimized hyperparameters
[~, ~, fmu_opt, fs2_opt] = gp(hyp_opt, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);
fmu_opt( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot') == 0) = NaN;
% plot the figure
figure
hold on
% surf(X1_plot, X2_plot, reshape(fmu_opt, ng_plot(2), ng_plot(1)), 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
% 2. geo
R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(fmu_opt, ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end


% figure properties

set(gcf,'Color','w');
if flag_save_fig
    export_fig ./FigPredictMeanFcnOptHyper.png -r300;
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcnOptHyper','-dpdf','-r300')
end
hold off


%% plot the original surface

[~, fmu_plot2] = getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot', sPar.sigma_n);

% plot the figure
figure
hold on
% surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')

R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(fmu_plot2, ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end


% figure properties
set(gcf,'Color','w');
if flag_save_fig
    export_fig('./FigTrueFcn.png');
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigTrueFcn','-dpdf','-r0')
end
hold off


%% plot map and POI and sensing positions using geo
if 1
    figure
    hold on
    [~, fmu_plot2] = getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot', sPar.sigma_n);
    
    R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
    Z = reshape(fmu_plot2, ng_plot(2), ng_plot(1));
    hg = worldmap(Z,R);
    hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
    hg.Children(12).LineStyle = ':';
    hg.Children(12).LineWidth = 0.5;
    for i=1:8
        hg.Children(i).FontSize = 14;
    end

    % show the subworkspace
    
    p1 = geopoint([ws.x3(2), ws.x4(2)], [ws.x3(1), ws.x4(1)]);
    geoshow(p1.Latitude, p1.Longitude,'LineStyle','--','Color','w','LineWidth',1);
    xplot = linspace( ws.x1(1), ws.x2(1), 100);
%     xplot = [ ws.x1(1), ws.x3(1), ws.x2(1)];
    yplot = (ws.x2(2) - ws.x1(2))/(ws.x2(1) - ws.x1(1))*(xplot - ws.x1(1)) + ws.x1(2);
    p2 = geopoint(yplot,xplot);
    geoshow(p2.Latitude, p2.Longitude,'LineStyle','--','Color','w','LineWidth',1);
    
    % plot sensing locations
    p = geopoint( ws.Xs(2,:), ws.Xs(1,:) ); % p = geopoint(y,x);
    hgs = geoshow(p.Latitude, p.Longitude, 'LineStyle','none','Marker','.','Color','r','MarkerSize',15);
    
    % plot points of interests
    p = geopoint( ws.Xi(2,:), ws.Xi(1,:) ); % p = geopoint(y,x);
    hgi = geoshow(p.Latitude, p.Longitude, 'LineStyle','none','Marker','d',...
        'Color',rgb('Black'),'MarkerSize',4, 'MarkerFaceColor',rgb('Black'));
    
    % annotate workspace, textm(lat,lon,string)
    textm(30,-120,'$\mathcal{W}_1$','Interpreter','latex','FontSize',15)
    textm(45,-130,'$\mathcal{W}_2$','Interpreter','latex','FontSize',15)
    textm(30,-77,'$\mathcal{W}_3$','Interpreter','latex','FontSize',15)
    textm(48,-77,'$\mathcal{W}_4$','Interpreter','latex','FontSize',15)
    
    
    % figure properties
    c = colorbar;
    c.Label.String = '$f(\mathbf{x})$'; c.Label.Interpreter = 'latex';
    c.Label.FontSize = 15;
    legend([hgs, hgi],{': $\mathcal{S}$',': $\mathcal{X}$'},'Interpreter','latex','FontSize',15);
    set(gca,'FontSize',15);
    set(gcf,'Color','w');
    if 0
        export_fig ./FigMapGeoV3.png -r500
        export_fig ./FigMapGeoV3.pdf -r500
    end
    hold off
end



%% plot error between predicted mean function and ground truth function
figure
hold on
% surf(X1_plot, X2_plot, reshape(abs(fmu_plot2-fmu_plot), ng_plot, ng_plot), 'FaceAlpha',1, 'LineStyle','none','FaceColor','interp');

R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(abs(fmu_plot2-fmu_plot'), ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end

% figure properties
set(gcf,'Color','w'); 
if flag_save_fig
    export_fig ./FigPredictError.png -r300;
    export_fig ./FigPredictError.pdf;
end
hold off


%% plot error between predicted mean fun with optimized hyper-par and ground truth fun
figure
hold on

R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(abs(fmu_plot2-fmu_opt'), ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end

% figure properties

set(gcf,'Color','w');

if flag_save_fig
    export_fig ./FigPredictErrorOptHyper.png -r300;
    export_fig ./FigPredictErrorOptHyper.pdf;
end
hold off










