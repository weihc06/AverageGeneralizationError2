%% add necessary libraries
addpath('../');
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

flag_save_fig = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');


%% plot workspace, and measurement sequence
figure

hold on

ng_plot = [140, 60]*2;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, fmu_plot2] = getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot', sPar.sigma_n);

% plot the figure

% surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')

R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
Z = reshape(fmu_plot2, ng_plot(2), ng_plot(1));  
hg = worldmap(Z,R);
hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
hg.Children(12).LineStyle = ':';
hg.Children(12).LineWidth = 0.5;
for i=1:8
    hg.Children(i).FontSize = 14;
end

% plot sensing locations
for i=1:sPar.ns
%     scatter3(ws.Xs(1,sensor_age(i).Mo==2), ws.Xs(2,sensor_age(i).Mo==2), ones(1,sum(sum(sensor_age(i).Mo==2)))*100, 200, clr_seq_ws(i,:),'.');
    p1 = geopoint(ws.Xs(2,sensor_age(i).Mo==2), ws.Xs(1,sensor_age(i).Mo==2));
    hgsl(i) = geoshow(p1.Latitude, p1.Longitude,'LineStyle','none','Marker','.','Color',clr_seq_ws(i,:),'MarkerSize',15);
end

% plot measurement sequence
for i=1:sPar.ns
    nm = 10;
%     plot3(sensor_age(i).Xl(1,1:nm), sensor_age(i).Xl(2,1:nm), ones(1,min(nm,length(sensor_age(i).yl)))*100);
    p1 = geopoint(sensor_age(i).Xl(2,1:nm), sensor_age(i).Xl(1,1:nm));
    hgms(i) = geoshow(p1.Latitude, p1.Longitude,'LineStyle','-',...
        'Color',clr_seq_ws(i,:),'MarkerSize',15,'LineWidth',1.5);
end


% Create text (a)
text('Clipping','on','FontSize',15,'Interpreter','latex',...
    'String','$(a)$',...
    'Position',[-2816608.49786174 5682171.96607389 0],...
    'Visible','on');

% figure properties
set(gcf,'Color','w');
legend([hgsl(:); hgms(:)],{'$\mathcal{S}_1$','$\mathcal{S}_2$','$\mathcal{S}_3$','$\mathcal{S}_4$',...
    '$\mathbf{x}_1(k)$','$\mathbf{x}_2(k)$','$\mathbf{x}_3(k)$','$\mathbf{x}_4(k)$'},...
    'FontName','Times','FontSize',15,'Interpreter','latex',...
    'Position',[0.810590273435336 0.293616729804635 0.183457344837693 0.480952367328463])

if 1
    saveas(gcf,'./FigMeasureSeqGdyMap.fig');
    export_fig './FigMeasureSeqGdyMap.png' -r500;
end
hold off