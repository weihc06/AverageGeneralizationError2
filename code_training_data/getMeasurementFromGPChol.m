function [yt, ft] = getMeasurementFromGPChol(covfunc, hyp_cov, L, alpha, Xr, Xt, sigma_n)
% [yt, ft] = getMeasurementFromGPChol(covfunc, hyp_cov, L, alpha, Xr, Xt, sigma_n)
% yt = getMeasurementFromGPChol(covfunc, hyp_cov, L, alpha, Xr, Xt, sigma_n)
%
% Functionality: This function returns the predictive mean of a GP model,
% where the training data are represented by cholesky factorization
% Input:
%    covfunc: covariance function of true GP process
%    hyp_cov: hyperparameters of true GP process
%    L: cholesky factorization of input covariance matrix
%    alpha: L'\(L\yr) = K^{-1}y
%    Xr: 2xnr matrix of training input data
%    Xt: 2xnt matrix of test input data
%    sigma_n: standard deviation of measurement noise (white Gaussian
%    distributed)
% Output:
%    yt: a vector of noisy measurements at locations in Xt
% Copyright (c) 2010-2016, Duke University 

    Ctr = feval(covfunc, hyp_cov, Xt', Xr');
    ft = Ctr * alpha; % mean of GP at test points
    yt = ft + normrnd(0, sigma_n, size(ft,1), size(ft,2));

end



