clear
clc

%%
file_name = './oscar-filter-monthly-574f00f0250b4.nc';

% open nc file, read only
ncid = netcdf.open(file_name,'NOWRITE');

% get cdf file information
[ndims,nvars,ngatts,unlimdimid] = netcdf.inq(ncid);

% get information about variables
for i=1:nvars
    [varname,xtype,dimids,natts] = netcdf.inqVar(ncid,i-1);
    disp(['ID: ', num2str(i-1), ', Name: ',varname]);
end

% get variable values
nc.lat = netcdf.getVar(ncid, 2);
nc.lon = netcdf.getVar(ncid, 3);
nc.u = netcdf.getVar(ncid,4);
nc.v = netcdf.getVar(ncid,5);

%% plot data
quiver(nc.lon,nc.lat,nc.u',nc.v');




