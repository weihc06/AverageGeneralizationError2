function varargout = getMeasurementFromRealData(data, dp, Xt, varargin)
% [yt, ft] = getMeasurementFromRealData(data, data_property, xt, sigma_n)
% [yt] = getMeasurementFromRealData(data, data_property, xt, sigma_n)
% if_data = getMeasurementFromRealData(data, data_property, xt)
%
% Functionality: This function returns the noisy measurement and real data
% Input:
%    data: data
%    dp: struct with field
%       x0  % origin of workspace
%       Lx  % X, Y length of workspace
%       Ly 
%       dx 
%       dy 
%    Xt: 2xnt matrix of test input data
%    sigma_n: standard deviation of measurement noise (white Gaussian
%    distributed)
% Output:
%    yt: a vector of noisy measurements at locations in Xt
% Copyright (c) 2010-2016, Duke University 
    
    nt = size(Xt,2);

    idx = ceil( (Xt - repmat( dp.x0', 1, nt)) ./ repmat([dp.dx; dp.dy], 1, nt) );

    if isempty( varargin )
        if_data = zeros(1, nt);
        for i=1:nt
            try
            if isnan( data( idx(1,i), idx(2,i) ) )
                if_data(i) = 0;
            else
                if_data(i) = 1;
            end
            catch
                idx;
            end
        end
        varargout{1} = if_data;
    else
        sigma_n = varargin{1};
        ft = zeros(1, nt);
        for i=1:nt
            try
                if idx(1,i) == 0; idx(1,i) = 1; end
                if idx(2,i) == 0; idx(2,i) = 1; end
            ft(i) = data( idx(1,i), idx(2,i) );
            catch
                idx;
            end
        end
        yt = ft + normrnd(0,sigma_n, 1, nt);
        
        varargout{1} = yt;
        if nargout > 1
            varargout{2} = ft;
        end
    end

end