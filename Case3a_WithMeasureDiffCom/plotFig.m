%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

flag_save_fig = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

figure
hold on


%% theoretic result for Uniformly Random 
tk = [];
sn_err_his = [];
for k=1:length(sn.err_his)
    tk = [tk k];
    sn_err_his = [sn_err_his sn.err_his{k}(1)];
    if length(sn.err_his{k})>1
        tk = [tk k];
        sn_err_his = [sn_err_his sn.err_his{k}(2)];
    end
end
h1 = plot(tk, sn_err_his,'LineWidth',2,'Color','k','LineStyle','--');
% individual sensors
% sensor_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor(i).err_his)
%         tk = [tk k];
%         sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(1)];
%         if length(sensor(i).err_his{k})>1
%             tk = [tk k];
%             sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_err_his{i},'LineWidth',2,'Color',clr_seq(1,:));
% end

%% simulation result for Uniformly Random 
% tk = [];
% sn_uni_err_his = [];
% for k=1:length(sn_uni.err_his)
%     tk = [tk k];
%     sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(1)];
%     if length(sn_uni.err_his{k})>1
%         tk = [tk k];
%         sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(2)];
%     end
% end
% h(2) = plot(tk, sn_uni_err_his,'LineWidth',2,'Color',clr_seq(2,:));
% % individual sensors
% sensor_uni_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor_uni(i).err_his)
%         tk = [tk k];
%         sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(1)];
%         if length(sensor_uni(i).err_his{k})>1
%             tk = [tk k];
%             sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_uni_err_his{i},'LineWidth',2,'Color',clr_seq(2,:));
% end

tcom = []; 
%% simulation result for Greedy Algorithm that maximizes AGE
% network
% figure; hold on;
tk = [];
sn_age_err_his = [];
for k=1:length(sn_age.err_his)
    tk = [tk k];
    sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(1)];
    if length(sn_age.err_his{k})>1
        tk = [tk k];
        
        sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(2)];
    end
end
h3 = plot(tk, sn_age_err_his,'LineStyle','-','LineWidth',2,'Color',clr_seq(1,:));

% individual sensors
sensor_age_err_his = cell(1,sPar.ns);
for i=1%:sPar.ns
    tk = [];
    for k=1:length(sensor_age(i).err_his)
        tk = [tk k];
        sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(1)];
        if length(sensor_age(i).err_his{k})>1
            tk = [tk k];
            tcom(end+1) = k;
            sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(2)];
        end
    end
    h31 = plot(tk, sensor_age_err_his{i},'LineWidth',2,'Color',ORANGE);
end


%% simulation result for Greedy Algorithm that maximizes Entropy

tk = [];
sn_epy_err_his = [];
for k=1:length(sn_epy.err_his)
    tk = [tk k];
    sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(1)];
    if length(sn_epy.err_his{k})>1
        tk = [tk k];
        sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(2)];
    end
end
h4 = plot(tk, sn_epy_err_his,'LineWidth',2,'Color',FORESTGREEN,'LineStyle','-.');
% % individual sensors
% sensor_epy_err_his = cell(1,sPar.ns);
% for i=1%:sPar.ns
%     tk = [];
%     for k=1:length(sensor_epy(i).err_his)
%         tk = [tk k];
%         sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(1)];
%         if length(sensor_epy(i).err_his{k})>1
%             tk = [tk k];
%             sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_epy_err_his{i},'LineWidth',2,'Color',clr_seq(4,:));
% end

hc = scatter(tcom, ones(1,length(tcom))*0.005,30,clr_seq(1,:),'o');
for t = tcom
    plot([t,t],[0 1],'-.','Color',clr_seq(1,:));
end
%% labels

box on
legend([h1,h31, h3, h4, hc],{'$\hat{\epsilon}_u(k)$','$\epsilon_g(Q_1,k)$',...
    '$\epsilon_g(Q,k)$','$\epsilon_e(Q,k)$','communication time'},'Interpreter','latex')

xlabel('time step, $k$','Interpreter','latex');
ylabel('$\hat{\epsilon}$', 'Interpreter','latex');
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off



%% plot regression result

% process all measurement inputs and outputs
X_new_plot = [];
y_new_plot = [];

sensor_plot = sensor_age;   
sn_plot = sn_age;

for i=1:length(sensor_plot)
    X_new_plot = [X_new_plot, sensor_plot(i).X_new];
    y_new_plot = [y_new_plot, sensor_plot(i).y_new];
end

Xa_plot = [sn_plot.Xa, X_new_plot];
ya_plot = [sn_plot.ya, y_new_plot];
    

ng_plot = 20;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot, fs2_plot] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);

% plot the figure
figure
hold on
surf(X1_plot, X2_plot, reshape(fmu_plot, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
text(0, 100, 40,'(b)','Interpreter','latex','FontSize',15)

xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$\hat{f}(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
legend({ 'Predicted Mean Fun', 'Training Data'},'Position',[0.6,0.65,0.3,0.15],'Interpreter','latex'); 
axis equal
view(-79,12) 
if flag_save_fig
    export_fig('./FigPredictMeanFcnV2.png');
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcnV2','-dpdf','-r0')
end
hold off

%% optimize hyper-parameter

hyp_opt = minimize(GP_Par.hyp, @gp, -100, @infExact, [], ...
    GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot'); % optimized hyperparameters
[~, ~, fmu_opt, fs2_opt] = gp(hyp_opt, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);

% plot the figure
figure
hold on
surf(X1_plot, X2_plot, reshape(fmu_opt, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
text(0, 95, 40,'(c)','Interpreter','latex','FontSize',15)
% figure properties
xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$\hat{f}(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function with Optimized Hyper-Par'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
legend({ 'Predicted Mean Fun', 'Training Data'},'Position',[0.55,0.7,0.3,0.15]); 
% axis tight
view(-79,12)
zlim([-40 40]);
if flag_save_fig
    export_fig ./FigPredictMeanFcnOptHyper.png -r300;
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcnOptHyperV2','-dpdf','-r300')
end
hold off


%% plot the original surface

ng_plot = 20;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot2, fs2_plot2] = gp(GP_Par0.hyp, @infExact, [], GP_Par0.covfunc, ...
    GP_Par0.likfunc, GP_Par0.Xr', GP_Par0.yr', xg_plot);

% plot the figure
figure
hold on
surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
text(0, 95, 40,'(a)','Interpreter','latex','FontSize',15)
% figure properties
xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$f(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
% legend({ 'Ground-truth Fun', 'Training Data'},'Position',[0.6,0.65,0.3,0.15],'Interpreter','latex'); 
legend({ 'Ground-truth Fun'},'Position',[0.6,0.70,0.3,0.075],'Interpreter','latex'); 

% axis equal
view(-79,12) 
if flag_save_fig
    export_fig('./FigTrueFcn.png');
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigTrueFcnV3','-dpdf','-r0')
end
hold off

%% plot workspace, and measurement sequence
figure

hold on

surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'LineStyle','none','FaceColor','interp');
% scatter3(ws.Xs(1,:), ws.Xs(2,:), ones(1,size(ws.Xs,2))*100, 200, rgb('Red'),'.');

for i=1:sPar.ns
    scatter3(ws.Xs(1,sensor_age(i).Mo==2), ws.Xs(2,sensor_age(i).Mo==2), ones(1,sum(sum(sensor_age(i).Mo==2)))*100, 200, clr_seq_ws(i,:),'.');
end

% plot measurement sequence
% for i=1:sPar.ns
%     nm = 10;
%     plot3(sensor_age(i).Xl(1,1:nm), sensor_age(i).Xl(2,1:nm), ones(1,min(nm,length(sensor_age(i).yl)))*100);
% end

plot3([ws.Lx/2, ws.Lx/2], [0, ws.Ly], [100 100],'Color','k','LineWidth',1,'LineStyle','--')
plot3([0, ws.Lx], [ws.Ly, ws.Ly]/2, [100 100],'Color','k','LineWidth',1,'LineStyle','--')

% figure properties
box on
axis equal
axis tight
xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
% title({'Workspace'});
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
% legend({ 'Ground-truth Fun', 'Sensing Locations'},'Location','northeastoutside'); 
hc = colorbar;
hc.Label.String = 'Function Value';
set(hc.Label,'FontName','Timesnewroman','Interpreter','latex')
hc.Label.FontSize = 15;
hc.Label.Rotation = 0;
hc.Label.Position = hc.Label.Position + [3 0 0 ];
hc.Label.Rotation = -90;
hc.Units = 'pixels';
hc.Position(4) = hc.Position(4)*0.6;
hc.Position(1) = hc.Position(1) + 20;
if flag_save_fig
    export_fig './FigWs.png' -r300;
    export_fig './FigWs.pdf';
end
hold off

%% plot error between predicted mean function and ground truth function
figure
hold on
surf(X1_plot, X2_plot, reshape(abs(fmu_plot2-fmu_plot), ng_plot, ng_plot), 'FaceAlpha',1, 'LineStyle','none','FaceColor','interp');
text(0, 95, 20,'(d)','Interpreter','latex','FontSize',15)

% figure properties
xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$|\hat{f}(\mathbf{x})-f(\mathbf{x})|$', 'Interpreter','latex');
% title({'Prediction Error'}, 'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
% axis equal
view(-79,12) 
if flag_save_fig
    export_fig ./FigPredictErrorV2.png -r300;
    export_fig ./FigPredictErrorV2.pdf;
end
hold off


%% plot error between predicted mean fun with optimized hyper-par and ground truth fun
figure
hold on
surf(X1_plot, X2_plot, reshape(abs(fmu_plot2-fmu_opt), ng_plot, ng_plot), 'FaceAlpha',1, 'LineStyle','none','FaceColor','interp');
text(0, 95, 0.8,'(e)','Interpreter','latex','FontSize',15)

% figure properties
xlabel('$x$ (m)','Interpreter','latex');
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$|\hat{f}(\mathbf{x})-f(\mathbf{x})|$', 'Interpreter','latex');
% title({'Prediction Error with Optimized Hyper-parameter'}, 'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
zlim([0 20]);
az = -79; el = 12;
view(az,el) 
if flag_save_fig
    export_fig ./FigPredictErrorOptHyperV2.png -r300;
    export_fig ./FigPredictErrorOptHyperV2.pdf;
end
hold off










