% In this test case, the Entropy sensors and the ARE sensors do not share
% the same information when communication, therefore, their curves have
% different jumps when communication
% This test case performs hyper-parameters optimization at the
% communication time.
% Copyright (c) 2010-2016, Duke University 

%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

result_case = 'R2';

%% set up configurations
run(['./setup',result_case,'.m']);

%% EAAGE without jump for sensor 1
% get approximated expected AGE for sensor 1 without communication by
% Uniform Random algorithm, E_{Q_1}[\hat{epsilon}_u[Q_1(k)]]
% This does not have jump in the curve

age_uni_thm_s1 = zeros(1,ws.tf);

% initialize the covariance matrix
sensor(1) = resetSensor(sensor(1));
sigma_f = exp(GP_Par.hyp.cov(2)); % make sure that sigma_f is not outdated when hyp is updated
for tt = 1:ws.tf
    [age_uni_thm_s1(tt), sensor(1).Cii, sensor(1).Cis, sensor(1).Css] = ...
        getAGENext(sensor(1).Cii, sensor(1).Cis, sensor(1).Css, sensor(1).Mo, sPar.sigma_n, sigma_f);
end

flag_age_uni_thm_s1 = 0;
if flag_age_uni_thm_s1
    if exist('fig_age','var')
        figure(fig_age);
    else
        fig_age = figure;
    end
    hold on
    plot(1:ws.tf, age_uni_thm_s1)
    xlabel('time step, $k$','Interpreter','latex');
    ylabel('$\hat{\epsilon}_u(k)$', 'Interpreter','latex');
    title({'Approximated Expected-AGE for Sensor 1'; ...
        'without communication and Uniform Random Algorithm'},...
        'FontWeight','normal');
    set(gca,'FontSize',15,'FontName','Timesnewroman');
    set(gcf,'Color','w');
    hold off
end

%% EAAGE without jump for sensor network
% get approximated expected AGE for sensor network always with
% communication by Uniform random algorithm, E_{Q}[\hat{epsilon}_u[Q(k)]]
% expected AGE of the whole workspace, when there is always communication
% This does not have jump in the curve

age_uni_thm_sn = zeros(1,ws.tf);

% initialize the covariance matrix
sn = resetSensor(sn);
sigma_f = exp(GP_Par.hyp.cov(2)); % make sure that sigma_f is not outdated when hyp is updated
% construct Mostruct for the sensor network
MoSn = cell(1,sPar.ns);
for i=1:sPar.ns
    MoSn{i} = sensor(i).Mo;
end
for tt=1:ws.tf
    [age_uni_thm_sn(tt), sn.Cii, sn.Cis, sn.Css] = ...
        getAGENextSN(sn.Cii, sn.Cis, sn.Css, MoSn, sPar.sigma_n, sigma_f);
end

flag_age_uni_thm_sn = 0;
if flag_age_uni_thm_sn
    if exist('fig_age','var')
        figure(fig_age);
    else
        fig_age = figure;
    end
    hold on
    plot(1:ws.tf, age_uni_thm_sn)
    xlabel('time step, $k$','Interpreter','latex');
    ylabel('$\hat{\epsilon}_u(k)$', 'Interpreter','latex');
    title({'Approximated Expected-AGE for Sensor 1 and SN'; ...
        'without communication and Uniform Random Algorithm'},...
        'FontWeight','normal');
    set(gca,'FontSize',15,'FontName','Timesnewroman');
    set(gcf,'Color','w');
    hold off
end

%% Simulation Results with Hyper-paramters Optimization
% Simulation result by Greedy algorithm that maximizes the entropy or the
% AGE. The simulation is conducted such that the sensors by different
% algorithms communicate at the same time for comparision.

% Initialize the covariance matrices
sn = resetSensor(sn);                 % theoretic result for Uniformly Random 
sensor = resetSensor(sensor);         % theoretic result for Uniformly Random 
sn_uni = resetSensor(sn_uni);         % simulation result for Uniformly Random 
sensor_uni = resetSensor(sensor_uni); % simulation result for Uniformly Random 
sn_age = resetSensor(sn_age);         % simulation result for Greedy Algorithm that maximizes AGE
sensor_age = resetSensor(sensor_age); % simulation result for Greedy Algorithm that maximizes AGE
sn_epy = resetSensor(sn_epy);         % simulation result for Greedy Algorithm that maximizes Entropy
sensor_epy = resetSensor(sensor_epy); % simulation result for Greedy Algorithm that maximizes Entropy

sigma_f = exp(GP_Par.hyp.cov(2)); % make sure that sigma_f is not outdated when hyp is updated
sigma_n = sPar.sigma_n;

% construct Mostruct for the sensor network
MoSn  = cell(1,sPar.ns);
MoUni = cell(1,sPar.ns);
MoAGE = cell(1,sPar.ns);
MoEpy = cell(1,sPar.ns);
for i=1:sPar.ns
    MoSn{i} = sensor(i).Mo;
end


for k = 1:ws.tf % simulation time
    disp(k);
    % get optimal sensing location encoded by Mo or MoNext (MoCurrent)
    % update new sensing location list
    for i=1:sPar.ns
        [MoUni{i}, sensor_uni(i).X_new(:,end+1)] = getSenPosMoXUni(sensor_uni(i).Mo, ws.Xs, sensor_uni(i).idx1, k);
        [MoAGE{i}, sensor_age(i).X_new(:,end+1)] = getSenPosMoXAGE(sensor_age(i).Mo, sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css, sigma_n, ws.Xs, sensor_age(i).idx1, k);
        [MoEpy{i}, sensor_epy(i).X_new(:,end+1)] = getSenPosMoXEpy(sensor_epy(i).Mo, sensor_epy(i).Css, ws.Xs, sensor_epy(i).idx1, k); 
        sensor_uni(i).Xl(:,end+1) = sensor_uni(i).X_new(:,end); % local information
        sensor_age(i).Xl(:,end+1) = sensor_age(i).X_new(:,end);
        sensor_epy(i).Xl(:,end+1) = sensor_epy(i).X_new(:,end);
    end
    
    % get noisy measurements, try duplicate measurements first
    for i=1:sPar.ns
        sensor_uni(i).y_new(end+1) = getMeasurementFromGPChol(GP_Par0.covfunc, GP_Par0.hyp.cov, GP_Par0.L, ...
            GP_Par0.alpha, GP_Par0.Xr, sensor_uni(i).X_new(:,end), sPar.sigma_n);
        sensor_age(i).y_new(end+1) = getMeasurementFromGPChol(GP_Par0.covfunc, GP_Par0.hyp.cov, GP_Par0.L, ...
            GP_Par0.alpha, GP_Par0.Xr, sensor_age(i).X_new(:,end), sPar.sigma_n);
        sensor_epy(i).y_new(end+1) = getMeasurementFromGPChol(GP_Par0.covfunc, GP_Par0.hyp.cov, GP_Par0.L, ...
            GP_Par0.alpha, GP_Par0.Xr, sensor_epy(i).X_new(:,end), sPar.sigma_n);
        sensor_uni(i).yl(end+1) = sensor_uni(i).y_new(end); % local information
        sensor_age(i).yl(end+1) = sensor_age(i).y_new(end);
        sensor_epy(i).yl(end+1) = sensor_epy(i).y_new(end);
    end
    
    % get age for every sensor and for the network
    for i=1:sPar.ns
        % theoretic result for Uniformly Random
        [sensor(i).err_his{k}, sensor(i).Cii, sensor(i).Cis, sensor(i).Css] ...
            = getAGENext(sensor(i).Cii, sensor(i).Cis, sensor(i).Css, sensor(i).Mo, sigma_n, GP_Par_age.sigma_f);
        
        % simulation result for Uniformly Random 
        [sensor_uni(i).err_his{k}, sensor_uni(i).Cii, sensor_uni(i).Cis, sensor_uni(i).Css] ...
            = getAGENext(sensor_uni(i).Cii, sensor_uni(i).Cis, sensor_uni(i).Css, MoUni{i}, sigma_n, GP_Par_uni.sigma_f);
        
        % simulation result for Greedy Algorithm that maximizes AGE
        [sensor_age(i).err_his{k}, sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css] ...
            = getAGENext(sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css, MoAGE{i}, sigma_n, GP_Par_age.sigma_f);
        
        % simulation result for Greedy Algorithm that maximizes Entropy
        [sensor_epy(i).err_his{k}, sensor_epy(i).Cii, sensor_epy(i).Cis, sensor_epy(i).Css] ...
            = getAGENext(sensor_epy(i).Cii, sensor_epy(i).Cis, sensor_epy(i).Css, MoEpy{i}, sigma_n, GP_Par_epy.sigma_f);
    end
    % theoretic result for Uniformly Random
    [sn.err_his{k}, sn.Cii, sn.Cis, sn.Css] = getAGENextSN(sn.Cii, sn.Cis, sn.Css, MoSn, sigma_n, GP_Par_age.sigma_f);
    % simulation result for Uniformly Random 
    [sn_uni.err_his{k}, sn_uni.Cii, sn_uni.Cis, sn_uni.Css] = ...
        getAGENextSN(sn_uni.Cii, sn_uni.Cis, sn_uni.Css, MoUni, sigma_n, GP_Par_uni.sigma_f);    
    % simulation result for Greedy Algorithm that maximizes AGE
    [sn_age.err_his{k}, sn_age.Cii, sn_age.Cis, sn_age.Css] = ...
        getAGENextSN(sn_age.Cii, sn_age.Cis, sn_age.Css, MoAGE, sigma_n, GP_Par_age.sigma_f);    
    % simulation result for Greedy Algorithm that maximizes Entropy
    [sn_epy.err_his{k}, sn_epy.Cii, sn_epy.Cis, sn_epy.Css] = ...
        getAGENextSN(sn_epy.Cii, sn_epy.Cis, sn_epy.Css, MoEpy, sigma_n, GP_Par_epy.sigma_f);    
    
    % get accumulate err between the learning curve by Uniform Random Algorithm
    % and the simulation result by AGE algorithm to determine whether to communicate
    accErr = max( getAccErr( sn, sensor_age ) );
    
    % if err > threshold, then communicate and reset ws.tc
    if accErr > sPar.comTh
        [sn_age, sensor_age, GP_Par_age] = comAmongSensorXOptPar(sn_age, sensor_age, k, GP_Par_age, ws, GP_Par_age.n_feval);        
        [sn,     sensor]                 = comAmongSensorDiffSNV2(sn_age, sn, sensor, k);
        
        [sn_uni, sensor_uni, GP_Par_uni] = comAmongSensorXOptPar(sn_uni, sensor_uni, k, GP_Par_uni, ws, GP_Par_uni.n_feval);
        [sn_epy, sensor_epy, GP_Par_epy] = comAmongSensorXOptPar(sn_epy, sensor_epy, k, GP_Par_epy, ws, GP_Par_epy.n_feval);
    end    
end

%% run this to save diskspace, all data are recoverable
for i=1:sPar.ns
%     sensor(i).Mo = [];
%     sensor_uni(i).Mo = [];
%     sensor_age(i).Mo = [];
%     sensor_epy(i).Mo = [];
    %
    sensor(i).Xa = []; sensor(i).ya = []; % already saved in sn.Xa and sn.ya
    sensor_uni(i).Xa = []; sensor_uni(i).ya = [];
    sensor_age(i).Xa = []; sensor_age(i).ya = [];
    sensor_epy(i).Xa = []; sensor_epy(i).ya = [];
    %
    sensor(i).Cii0 = []; sensor(i).Cis0 = []; sensor(i).Css0 = [];
    sensor(i).Cii = []; sensor(i).Cis = []; sensor(i).Css = [];
    sensor_uni(i).Cii0 = []; sensor_uni(i).Cis0 = []; sensor_uni(i).Css0 = [];
    sensor_uni(i).Cii = []; sensor_uni(i).Cis = []; sensor_uni(i).Css = [];
    sensor_age(i).Cii0 = []; sensor_age(i).Cis0 = []; sensor_age(i).Css0 = [];
    sensor_age(i).Cii = []; sensor_age(i).Cis = []; sensor_age(i).Css = [];
    sensor_epy(i).Cii0 = []; sensor_epy(i).Cis0 = []; sensor_epy(i).Css0 = [];
    sensor_epy(i).Cii = []; sensor_epy(i).Cis = []; sensor_epy(i).Css = [];
    
end
sn.Mo = []; sn_uni.Mo = []; sn_age.Mo = []; sn_epy.Mo = [];
sn.Cii0     = [];     sn.Cis0 = [];     sn.Css0 = [];
sn.Cii      = [];     sn.Cis  = [];     sn.Css  = [];
sn_uni.Cii0 = []; sn_uni.Cis0 = []; sn_uni.Css0 = [];
sn_uni.Cii  = []; sn_uni.Cis  = []; sn_uni.Css  = [];
sn_age.Cii0 = []; sn_age.Cis0 = []; sn_age.Css0 = [];
sn_age.Cii  = []; sn_age.Cis  = []; sn_age.Css  = [];
sn_epy.Cii0 = []; sn_epy.Cis0 = []; sn_epy.Css0 = [];
sn_epy.Cii  = []; sn_epy.Cis  = []; sn_epy.Css  = [];
ws.sx_s = []; ws.sy_s = []; ws.sx_i = []; ws.sy_i = [];
K = [];
L = [];
MoAGE = {}; MoEpy = {}; MoSn = {}; MoUni = {};
Sx = []; Sy = []; Xtmp = []; Ytmp = [];
GP_Par0.L = []; GP_Par0.alpha = [];

%% plot figures

run(['./plotFig',result_case,'.m']);






