% This scripture sets up the configurations of the test case
% Author: Hongchuan Wei
% Copyright (c) 2010-2016, Duke University 

% decreasing the number of poi can produce better results
% when the poi and the sensing positions do not overlap, the results are
% better
%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

rng(0);

%% Workspace/Environment parameters

%%%%%%%%%%%%      Parameters of geometry of workspace             %%%%%%%%%
ws.type = 'square'; % type of workspace
ws.Lx = 100; % X, Y length of workspace
ws.Ly = 100;
ws.x0 = [0;0]; % origin of workspace

%%%%%%%%%%%%      Parameters of accessible sensing locations      %%%%%%%%%
ws.sensing_dis = 'grid'; % distribution of sensing locations
ws.nx_s = 10; % number of rows of accessible sensing locations
ws.ny_s = 10; % number of columns of accessible sensing locations
ws.n_s = ws.nx_s * ws.ny_s; % the number of accessible sensing locations in total
ws.dx_s = ws.Lx/(ws.nx_s+1); % x interval of sensing locations
ws.dy_s = ws.Ly/(ws.ny_s+1); % y interval of sensing locations
[Sx, Sy] = meshgrid(ws.x0(1)+ws.dx_s:ws.dx_s:ws.Lx-ws.dx_s, ws.x0(2)+ws.dy_s:ws.dy_s:ws.Ly-ws.dy_s);
ws.sx_s = reshape(Sx',1,[]);
ws.sy_s = reshape(Sy',1,[]);
ws.Xs = [ws.sx_s; ws.sy_s]; % (d=2) x n sensing locations


%%%%%%%%%%%%      Parameters of test points/points of interest   %%%%%%%%%%
% ws.poi_dis = 'grid'; % distribution of points of interest
% ws.sx_i = ws.sx_s(1:1:end);
% ws.sy_i = ws.sy_s(1:1:end);
% ws.Xi = [ws.sx_i; ws.sy_i]; % 2xn points of interest, for the laziness
ws.poi_dis = 'random';
ws.n_i = 100;
ws.sx_i = rand(1,ws.n_i)*ws.Lx + ws.x0(1);
ws.sy_i = rand(1,ws.n_i)*ws.Ly + ws.x0(2);
ws.Xi = [ws.sx_i; ws.sy_i];

%%%%%%%%%%%%      parameters of simulation time                %%%%%%%%%%%%
ws.tf = 50; % simulation total time, (ws.dt = 1, implied)

%% Gaussian Process Parameters for Real Data, 'r' stands for real
% Used by getMeasurementFromGP.m

n_x_r = 11; % number of training data points
n_y_r = 11;

% Gaussian Process parameters
GP_Par0.covfunc = @covSEiso; % squared exponential cov
GP_Par0.hyp.cov = [log(10); log(1)];  % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
GP_Par0.likfunc = @likGauss; % Gaussian likelihood function/measurement func
GP_Par0.sigma_n_r = sqrt(1); % standard deviation of measurement noise
GP_Par0.hyp.lik = log(GP_Par0.sigma_n_r); % log(sigma_n)

% get training inputs and outputs on grid within the workspace
[Xtmp, Ytmp] = meshgrid(linspace(ws.x0(1),ws.Lx,n_x_r), linspace(ws.x0(2),ws.Ly,n_y_r));
GP_Par0.Xr = [Xtmp(:)'; Ytmp(:)']; % 2xn training inputs, just to be consistent
GP_Par0.yr = (GP_Par0.Xr(1,:)-ws.Lx/2).*(GP_Par0.Xr(2,:)-ws.Ly/2)/100 ...
    + normrnd(0, GP_Par0.sigma_n_r, 1, size(GP_Par0.Xr,2));  % inducing output data

% optimized hyperparameters
hypt = minimize(GP_Par0.hyp, @gp, -100, @infExact, [], GP_Par0.covfunc, GP_Par0.likfunc, GP_Par0.Xr', GP_Par0.yr'); 
GP_Par0.hyp = hypt;

K = feval(GP_Par0.covfunc, GP_Par0.hyp.cov, GP_Par0.Xr') + eye(size(GP_Par0.Xr,2))*(GP_Par0.sigma_n_r)^2;
L = chol(K,'lower');
GP_Par0.L = L;
GP_Par0.alpha = L'\(L\GP_Par0.yr');

%% Gaussian Process Parameters for Latent Variables/Function/GP process

% latent function related 
GP_Par.covfunc = @covSEiso; % squared exponential cov
GP_Par.hyp.cov = [log(sqrt(100)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T

% measurement/observation/training data related
GP_Par.likfunc = @likGauss; % Gaussian likelihood function/measurement func
GP_Par.sigma_n = sqrt(0.1); % standard deviation of measurement noise
GP_Par.hyp.lik = log(GP_Par.sigma_n); % log(sigma_n)
GP_Par.sigma_f = exp(GP_Par.hyp.cov(2));
GP_Par.n_feval = 7;

GP_Par_age = GP_Par;
GP_Par_epy = GP_Par;
GP_Par_uni = GP_Par;

%% Sensor paparameters

sPar.ns = 4; % number of sensors
sPar.comTh = 0.4; % communication threshold
sPar.sigma_n = GP_Par.sigma_n; % measurement noise standard variance

%% Sensor Network Structs for Theoretical Curves, Uniform Random

%%%%%%%%%%%%      Parameters related to measurements/training data %%%%%%%%
%%%%%%%%%%%%      of the entire sensor network                     %%%%%%%%

% Mo: initial observation matrix/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
sn.Mo = ones(ws.nx_s, ws.ny_s)*2; % assume that all positions can be measured

%..........................................................................
% ....    test if the algorithm works when there are measurement     .... %
% ....    that are already obtained at the initial time              .... %
nO = 0; % floor(ws.ns/3); % running test of the algorithm
random_tmp = unidrnd( ws.nx_s, 2, nO);
for jj = 1:size(random_tmp,2)
    sn.Mo(random_tmp(1,jj), random_tmp(2,jj)) = 1;
end
%..........................................................................

% matrix of observed measurement positions, d x n matrix, where d is the
% dimension of input, n is the number of observations
sn.Xa0 = [ws.sx_s(find(sn.Mo == 1));...
          ws.sy_s(find(sn.Mo == 1))]; 
sn.Xa = sn.Xa0;
% get noisy measurements from an underlying GP process determined by
% GP_Par0
% ws.ya is a vector of 1 x n matrix/vector, containing all the noisy
% measurements
sn.ya0 = getMeasurementFromGPChol(GP_Par0.covfunc, GP_Par0.hyp.cov, ...
    GP_Par0.L, GP_Par0.alpha, GP_Par0.Xr, sn.Xa0, sPar.sigma_n);
sn.ya = sn.ya0;


%%%%%%%%%%%%      get initial covariance matrices              %%%%%%%%%%%%
% [varargout] = gp(hyp, inf, mean, cov, lik, x, y, xs, ys)
if ~isempty(sn.Xa0) 
    [~, ~, ~, fs2g] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, sn.Xa0', sn.ya0, ws.Xi');
    sn.Cii0 = fs2g;
    [~, ~, ~, fs2g] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, sn.Xa0', sn.ya0, ws.Xs');
    sn.Css0 = fs2g;
    
    Caa = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa0', sn.Xa0') + eye(size(sn.Xa0,2))*(GP_Par.sigma_n^2);
    Cis = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', ws.Xs');
    Cia = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', sn.Xa0');
    Cas = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa0', ws.Xs');    
    sn.Cis0 = Cis - Cia /Caa *Cas;
else
    % initial covariance matrices
    sn.Cii0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi'); % x: n by D matrix of inputs
    sn.Cis0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', ws.Xs'); % x: n by D matrix of inputs
    sn.Css0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xs');
end
% matrices without '0' are subject to change
sn.Cii = sn.Cii0;
sn.Cis = sn.Cis0;
sn.Css = sn.Css0;

sn.err_his = {}; % history of generalization errors
                 % assumed to be cell array since two values can occur at
                 % the time of cummunication
sn.tc = 0; % latest communication time


%% Other Sensor Network Structs for Experimental Results 
sn_uni = sn;         % simulation result for Uniformly Random 
sn_age = sn;         % simulation result for Greedy Algorithm that maximizes AGE
sn_epy = sn;         % simulation result for Greedy Algorithm that maximizes Entropy


%% Individual Sensors for Theoretical Results, Uniform Random Algorithm
for i=1:sPar.ns
    sensor(i) = sn;
end
for i=1:sPar.ns % additional features in subworkspace
    %   Mo: matrix stores which positions have been observed
    %       0: observed, but could not obtain measurement
    %       1: observed, and obtained measurement
    %       2: positions not checked yet
    sensor(i).Mo = zeros(ws.nx_s, ws.ny_s); % mask of sensing locations
    sensor(i).Xa0 = [];
    sensor(i).ya0 = [];
    sensor(i).Xa = sensor(i).Xa0; % 2xn matrix of inputs before last communication, all sensors'
    sensor(i).ya = sensor(i).ya0; % vector of outputs before last communication 
    sensor(i).Xl = sensor(i).Xa0; % 2xn matrix of inputs, local sensor's
    sensor(i).yl = sensor(i).ya0; 
    sensor(i).err_his = {}; % history of generalization errors
    sensor(i).X_new = [];  % vector of new observed positions/input 
    sensor(i).y_new = [];   % row vector of new measurements/observations/output data
end
% setup masks of sensing locations for every sensor
sensor(1).Mo(1:ws.nx_s/2, 1:ws.ny_s/2) = 2;
sensor(2).Mo(1+ws.nx_s/2:end, 1:ws.ny_s/2) = 2;
sensor(3).Mo(1:ws.nx_s/2, 1+ws.ny_s/2:end) = 2;
sensor(4).Mo(1+ws.nx_s/2:end, 1+ws.ny_s/2:end) = 2;
% the index of the first measurement in Mo, in order to make the initial
% measurement position the same
for i=1:sPar.ns
    Mo_i = find(sensor(i).Mo == 2);
    sensor(i).idx1 = Mo_i( ceil( length(Mo_i)/3 ));
end

%% Individual Sensors for Simulation Results
sensor_uni = sensor; % simulation result for Uniformly Random 
sensor_age = sensor; % simulation result for Greedy Algorithm that maximizes AGE
sensor_epy = sensor; % simulation result for Greedy Algorithm that maximizes Entropy


%% plot figures
flag_fig_GP0 = 0;
if flag_fig_GP0
    figGP0 = figure;
    hold on;
    scatter3(GP_Par0.Xr(1,:), GP_Par0.Xr(2,:), GP_Par0.yr, 100, rgb('Green'),'.'); % training data for GP0
    scatter3(sn.Xa0(1,:), sn.Xa0(2,:), sn.ya0, 100, rgb('Red'), '*'); % test GP for sensor network
    
    hype = [GP_Par0.hyp.cov; GP_Par0.hyp.lik];    % regression GP predictive mean
    % hype = [ hyp_cov
    %          log(sn)
    %          hyp_mean ]
    me = {@meanGPexact,@meanZero,@covSEiso, GP_Par0.Xr', GP_Par0.yr'};
    % 2) evaluate the function on x
    [Xtmp2, Ytmp2] = meshgrid(linspace(ws.x0(1),ws.Lx,2*n_x_r),linspace(ws.x0(2),ws.Ly,2*n_y_r));
    Xe = [Xtmp2(:)'; Ytmp2(:)']; % 2xn training inputs, just to be consistent
    fe = feval(me{:},hype, Xe');

    surf(Xtmp2, Ytmp2, reshape(fe, 2*n_x_r, 2*n_y_r),'FaceAlpha',0.5,'LineStyle','none');
    view(68,8);
    
    legend({'real data','test data','real GP'},'Location','NorthWest');
    axis equal
    set(gcf,'Color','w');
    hold off
end

flag_Xs_Mo_Direction = 0;
if flag_Xs_Mo_Direction
    figDirect = figure;
    MoTest = zeros(ws.nx_s, ws.ny_s);
    MoTest(3,5)=1;
    disp('I want sensing location with index (3,5) to be selected');
    idx_test = find(MoTest==1);
    
    hold on;
    plot(ws.sx_s, ws.sy_s, 'Marker','.');
    scatter(ws.sx_s(1), ws.sy_s(1));
    scatter(ws.sx_s(idx_test), ws.sy_s(idx_test),'x')
    xlabel('$x$','Interpreter','latex');
    ylabel('$y$', 'Interpreter','latex');
    title('Arrangement of sensing location in Css','FontWeight','normal');
    set(gca,'FontSize',15,'FontName','Timesnewroman');
    axis equal
    set(gcf,'Color','w');
    hold off;
end
