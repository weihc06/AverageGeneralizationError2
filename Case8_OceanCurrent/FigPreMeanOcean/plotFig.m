% close all

%% add necessary libraries
addpath('../');
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

flag_save_fig = 0;
flag_title = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

sigma_n = GP_Par0.sigma_n;



% %% hyper-par optimization
% n_opt_num = 10000;
% for i=1:sPar.ns
%     sensor_age(i).GP_Par.hyp1 = ...
%         minimize(sensor_age(i).GP_Par.hyp1, @gp, -n_opt_num, @infExact, [], ...
%         sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.likfunc, ...
%         sensor_age(i).X_new', sensor_age(i).y_new1'); % optimized hyperparameters
%     sensor_age(i).GP_Par.hyp2 = ...
%         minimize(sensor_age(i).GP_Par.hyp2, @gp, -n_opt_num, @infExact, [], ...
%         sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.likfunc, ...
%         sensor_age(i).X_new', sensor_age(i).y_new2'); % optimized hyperparameters
% end



%% plot map using surf

path_name2 = '..\..\code_training_data\ocean_data\';    
[fp.img, map] = imread([path_name2,'FigOceanMap2.png']);
fp.img = flipud(fp.img);
[xl,yl] = size(fp.img);
fp.x = [-188 -96]+3;
fp.y(1) = 17.5/24.5*20+1;
fp.y(2) = 65+1;


figure
hold on

image(fp.x, fp.y, fp.img);

fill([-160 -150 -150 -160],[20 20 30 30],[166, 201, 255]/255,...
    'LineStyle','none')

% quiver(nc.lon,nc.lat,nc.vx',nc.vy');
hq = quiver(xg_plot(:,1),xg_plot(:,2), fmu_opt1, fmu_opt2,'Color',clr_seq(2,:));   



% % plot sensing locations
% scatter3(ws.Xs(1,:), ws.Xs(2,:), 100*ones(1,size( ws.Xs,2) ), 200, rgb('Red'),'.')
% 
% % plot points of interests
% scatter3(ws.Xi(1,:), ws.Xi(2,:), 100*ones(1,size( ws.Xi,2) ), 25, rgb('Black'),'d','filled')

%%%%%%%%%%
xlabel('$x ~(^{\circ})$','Interpreter','latex','FontSize',15);
ylabel('$y ~(^{\circ})$','Interpreter','latex','FontSize',15);
legend(hq, {'$\hat{\mathbf{f}}(\mathbf{x})$'},'Interpreter','latex')


%%%%%%%%%
axis equal
axis tight
set(gca,'Xlim',[-160 -100],'YLim',[20, 60]);
grid on

set(gcf,'Color','w')
set(gca,'FontSize',15);
box on
if 1
    saveas(gcf,'./FigPreMeanOcean.fig');
    export_fig ./FigPreMeanOcean.png -r500
    export_fig ./FigPreMeanOcean.pdf
end
hold off


% %% plot true map using surf
% 
% path_name2 = '..\..\code_training_data\ocean_data\';    
% [fp.img, map] = imread([path_name2,'FigOceanMap2.png']);
% fp.img = flipud(fp.img);
% [xl,yl] = size(fp.img);
% fp.x = [-184 -96]-0.5;
% fp.y(1) = 17.5/24.5*20;
% fp.y(2) = 63;
% 
% 
% figure
% hold on
% 
% image(fp.x, fp.y, fp.img);
% 
% % quiver(nc.lon,nc.lat,nc.vx',nc.vy');
% quiver(xg_plot(:,1),xg_plot(:,2), fmu_plot_true1', fmu_plot_true2');            
% 
% % % plot sensing locations
% % scatter3(ws.Xs(1,:), ws.Xs(2,:), 100*ones(1,size( ws.Xs,2) ), 200, rgb('Red'),'.')
% % 
% % % plot points of interests
% % scatter3(ws.Xi(1,:), ws.Xi(2,:), 100*ones(1,size( ws.Xi,2) ), 25, rgb('Black'),'d','filled')
% 
% 
% %     legend({'Spatial Phenomenon','Sensing location','Points of interest'},'Location','southoutside')
% axis equal
% axis tight
% set(gca,'Xlim',[-160 -100],'YLim',[20, 60]);
% grid on
% box on
% set(gcf,'Color','w')
% set(gca,'FontSize',15);
% 
% if 0
%     saveas(gcf,'./FigPredictMeanFcnMap.fig');
%     export_fig ./FigPredictMeanFcnMap.png -r500
% end
% hold off


%%
% figure
% surf(X1_plot,X2_plot, reshape(fmu_plot_true1,ng_plot(2),ng_plot(1)))
% 
% figure
% surf(X1_plot,X2_plot, reshape(fmu_plot_true2,ng_plot(2),ng_plot(1)))


