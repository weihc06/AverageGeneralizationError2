%% add necessary libraries
addpath('../');
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

clr_seq =[
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

%%

figure 
hold on
uni_err = cell2mat(sensor_uni(1).err_his);
age_err = cell2mat(sensor_age(1).err_his);
epy_err = cell2mat(sensor_epy(1).err_his);

% plot( uni_err,'LineWidth',2);
% plot( age_err,'LineWidth',2,'Color',clr_seq(7,:));
% plot( epy_err,'LineWidth',2);
% 
x_scatter = [1:20:200, 200];
% scatter(x_scatter,uni_err(x_scatter),100,clr_seq(1,:),'LineWidth',2,'Marker','s');
% scatter(x_scatter,age_err(x_scatter),100,clr_seq(7,:),'LineWidth',2,'Marker','o');
% scatter(x_scatter,epy_err(x_scatter),100,clr_seq(3,:),'LineWidth',2,'Marker','^');

huni = plot(x_scatter,uni_err(x_scatter),'Color',clr_seq(1,:),'LineWidth',2,'Marker','s','MarkerSize',10);
hage = plot(x_scatter,age_err(x_scatter),'Color',clr_seq(7,:),'LineWidth',2,'Marker','o','MarkerSize',10);
hepy = plot(x_scatter,epy_err(x_scatter),'Color',clr_seq(3,:),'LineWidth',2,'Marker','^','MarkerSize',10);

%%%%%%%%%%%%%
xlabel('$k$','interpreter','latex','FontSize',15);
ylabel('$\epsilon(k)$ (m/s)','interpreter','latex','FontSize',15);
legend([hage, huni,hepy],{': GP-EKLD',': Random',': Entropy'},'Interpreter','latex','FontSize',15,...
        'Position',[0.569998844278744 0.697968256522739 0.291716597156796 0.187301582192618]);
%%%%%%%%%%%%%
box on
set(gcf,'Color','w');
set(gca,'FontSize',15);
hold off









 if 1
     saveas(gca,'./FigErrOcean.fig');
        export_fig ./FigErrOcean.png -r500
        export_fig ./FigErrOcean.pdf
        
%         set(gca,'Units','Inches');
%         pos = get(gca,'Position');
%         set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%         print(gcf,'./FigMapGeoV5','-dpdf','-r0')
    end