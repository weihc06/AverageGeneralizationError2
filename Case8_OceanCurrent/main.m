
% This test case use the Ocean Current data 
% with GPML package
% Copyright (c) 2010-2016, Duke University 

clear
close all
%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

%% set up configurations
run('./setup.m');


%% Simulation Results
% Simulation result by Greedy algorithm that maximizes the entropy or the
% AGE. The simulation is conducted such that the sensors by different
% algorithms communicate at the same time for comparision.

% Initialize the covariance matrices
sensor = resetSensor(sensor);         % theoretic result for Uniformly Random 
sensor_uni = resetSensor(sensor_uni); % simulation result for Uniformly Random 
sensor_age = resetSensor(sensor_age); % simulation result for Greedy Algorithm that maximizes AGE
sensor_epy = resetSensor(sensor_epy); % simulation result for Greedy Algorithm that maximizes Entropy

sigma_f = exp(GP_Par0.hyp1.cov(2)); % make sure that sigma_f is not outdated when hyp is updated
sigma_n = sPar.sigma_n;

for k = 1:ws.tf % simulation time
    disp(k);
    % get optimal sensing location encoded by Mo or MoNext (MoCurrent)
    % update new sensing location list
    for i=1:sPar.ns
        [MoUni{i}, sensor_uni(i).X_new(:,end+1)] = getSenPosMoXUni(sensor_uni(i).Mo, ws.Xs, sensor_uni(i).idx1, k);
        [MoAGE{i}, sensor_age(i).X_new(:,end+1)] = getSenPosMoXAGE(sensor_age(i).Mo, sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css, sigma_n, ws.Xs, sensor_age(i).idx1, k);
        [MoEpy{i}, sensor_epy(i).X_new(:,end+1)] = getSenPosMoXEpy(sensor_epy(i).Mo, sensor_epy(i).Css, ws.Xs, sensor_epy(i).idx1, k); 
    end
    
    % get noisy measurements, try duplicate measurements first
    for i=1:sPar.ns
        sensor_uni(i).y_new1(end+1) = getMeasurementFromRealData(ws.vx, ws, sensor_uni(i).X_new(:,end), sPar.sigma_n);
        sensor_uni(i).y_new2(end+1) = getMeasurementFromRealData(ws.vy, ws, sensor_uni(i).X_new(:,end), sPar.sigma_n);
        sensor_age(i).y_new1(end+1) = getMeasurementFromRealData(ws.vx, ws, sensor_age(i).X_new(:,end), sPar.sigma_n);
        sensor_age(i).y_new2(end+1) = getMeasurementFromRealData(ws.vy, ws, sensor_age(i).X_new(:,end), sPar.sigma_n);
        sensor_epy(i).y_new1(end+1) = getMeasurementFromRealData(ws.vx, ws, sensor_epy(i).X_new(:,end), sPar.sigma_n);  
        sensor_epy(i).y_new2(end+1) = getMeasurementFromRealData(ws.vy, ws, sensor_epy(i).X_new(:,end), sPar.sigma_n);  
    end
    
    if 1 %mod(k,10)~=0
        % get age for every sensor and for the network
        for i=1:sPar.ns
            
            % simulation result for Uniformly Random
            [sensor_uni(i).err_his{k}, sensor_uni(i).Cii, sensor_uni(i).Cis, sensor_uni(i).Css] ...
                = getAGENext(sensor_uni(i).Cii, sensor_uni(i).Cis, sensor_uni(i).Css, MoUni{i}, sigma_n, sigma_f);
            
            % simulation result for Greedy Algorithm that maximizes AGE
            [sensor_age(i).err_his{k}, sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css] ...
                = getAGENext(sensor_age(i).Cii, sensor_age(i).Cis, sensor_age(i).Css, MoAGE{i}, sigma_n, sigma_f);
            
            % simulation result for Greedy Algorithm that maximizes Entropy
            [sensor_epy(i).err_his{k}, sensor_epy(i).Cii, sensor_epy(i).Cis, sensor_epy(i).Css] ...
                = getAGENext(sensor_epy(i).Cii, sensor_epy(i).Cis, sensor_epy(i).Css, MoEpy{i}, sigma_n, sigma_f);
        end
    else
        n_opt_num = 10000;
        for i=1:sPar.ns
            %%
            % hyper-par optimization
            sensor_uni(i).GP_Par.hyp1 = ...
                minimize(sensor_uni(i).GP_Par.hyp1, @gp, -n_opt_num, @infExact, [], ...
                sensor_uni(i).GP_Par.covfunc, sensor_uni(i).GP_Par.likfunc, ...
                sensor_uni(i).X_new', sensor_uni(i).y_new1'); % optimized hyperparameters
            % get new covariance matrices according to optimized
            % hyper-parameters
            Caa = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, ...
                sensor_uni(i).X_new', sensor_uni(i).X_new') ...
                + eye(size(sensor_uni(i).X_new,2))*(GP_Par0.sigma_n^2);
            Cii = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, ws.Xi', ws.Xi');
            Css = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, ws.Xs', ws.Xs');
            
            Cis = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, ws.Xi', ws.Xs');
            Cia = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, ws.Xi', sensor_uni(i).X_new');
            Cas = feval(sensor_uni(i).GP_Par.covfunc, sensor_uni(1).GP_Par.hyp1.cov, sensor_uni(i).X_new', ws.Xs');
            sensor_uni(i).Cii = Cii - Cia /Caa * Cia';
            sensor_uni(i).Cis = Cis - Cia /Caa *Cas;
            sensor_uni(i).Css = Css - Cas'/ Caa* Cas;
            % get error
            sigma_f = exp(sensor_uni(i).GP_Par.hyp1.cov(2));
            sensor_uni(i).err_his{k} = trace(sensor_uni(i).Cii)/size(sensor_uni(i).Cii,1);
            
            %%
            % hyper-par optimization
            sensor_epy(i).GP_Par.hyp1 = ...
                minimize(sensor_epy(i).GP_Par.hyp1, @gp, -n_opt_num, @infExact, [], ...
                sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.likfunc, ...
                sensor_epy(i).X_new', sensor_epy(i).y_new1'); % optimized hyperparameters
            % get new covariance matrices according to optimized
            % hyper-parameters
            Caa = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(1).GP_Par.hyp1.cov, ...
                sensor_epy(i).X_new', sensor_epy(i).X_new') ...
                + eye(size(sensor_epy(i).X_new,2))*(GP_Par0.sigma_n^2);
            Cii = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.hyp1.cov, ws.Xi', ws.Xi');
            Css = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.hyp1.cov, ws.Xs', ws.Xs');
            
            Cis = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.hyp1.cov, ws.Xi', ws.Xs');
            Cia = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.hyp1.cov, ws.Xi', sensor_epy(i).X_new');
            Cas = feval(sensor_epy(i).GP_Par.covfunc, sensor_epy(i).GP_Par.hyp1.cov, sensor_epy(i).X_new', ws.Xs');
            sensor_epy(i).Cii = Cii - Cia /Caa * Cia';
            sensor_epy(i).Cis = Cis - Cia /Caa *Cas;
            sensor_epy(i).Css = Css - Cas'/ Caa* Cas;
            % get error
            sigma_f = exp(sensor_epy(i).GP_Par.hyp1.cov(2));
            sensor_epy(i).err_his{k} = trace(sensor_epy(i).Cii)/size(sensor_epy(i).Cii,1);
            
            
            %%
            % hyper-par optimization
            sensor_age(i).GP_Par.hyp1 = ...
                minimize(sensor_age(i).GP_Par.hyp1, @gp, -n_opt_num, @infExact, [], ...
                sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.likfunc, ...
                sensor_age(i).X_new', sensor_age(i).y_new1'); % optimized hyperparameters
            % get new covariance matrices according to optimized
            % hyper-parameters
            Caa = feval(sensor_age(i).GP_Par.covfunc, sensor_age(1).GP_Par.hyp1.cov, ...
                sensor_age(i).X_new', sensor_age(i).X_new') ...
                + eye(size(sensor_age(i).X_new,2))*(GP_Par0.sigma_n^2);
            Cii = feval(sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.hyp1.cov, ws.Xi', ws.Xi');
            Css = feval(sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.hyp1.cov, ws.Xs', ws.Xs');
            
            Cis = feval(sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.hyp1.cov, ws.Xi', ws.Xs');
            Cia = feval(sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.hyp1.cov, ws.Xi', sensor_age(i).X_new');
            Cas = feval(sensor_age(i).GP_Par.covfunc, sensor_age(i).GP_Par.hyp1.cov, sensor_age(i).X_new', ws.Xs');
            sensor_age(i).Cii = Cii - Cia /Caa * Cia';
            sensor_age(i).Cis = Cis - Cia /Caa *Cas;
            sensor_age(i).Css = Css - Cas'/ Caa* Cas;
            % get error
            sigma_f = exp(sensor_age(i).GP_Par.hyp1.cov(2));
            sensor_age(i).err_his{k} = trace(sensor_age(i).Cii)/size(sensor_age(i).Cii,1);
        end
    end  
end

%% mesh grids
ng_plot = floor( [60, 40] );
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

%% true function 
[~, fmu_plot_true1] = getMeasurementFromRealData(ws.vx_inter', ws, xg_plot', GP_Par0.sigma_n);
[~, fmu_plot_true2] = getMeasurementFromRealData(ws.vy_inter', ws, xg_plot', GP_Par0.sigma_n);

%% regression result with hyper-parameter optimization
ng_opt = floor( [60, 40] );
[X1_opt, X2_opt] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_opt(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_opt(2)));
Xa_opt_tmp = [X1_opt(:), X2_opt(:)];
Xa_opt = Xa_opt_tmp( getMeasurementFromRealData(ws.vx_inter', ws, Xa_opt_tmp') == 1, :)';
ya_opt1 = getMeasurementFromRealData(ws.vx_inter', ws, Xa_opt, GP_Par0.sigma_n);
ya_opt2 = getMeasurementFromRealData(ws.vy_inter', ws, Xa_opt, GP_Par0.sigma_n);

hyp_opt1 = minimize(sensor_age(1).GP_Par.hyp1, @gp, -10000, @infExact, [], ...
    sensor_age(1).GP_Par.covfunc, sensor_age(1).GP_Par.likfunc, Xa_opt', ya_opt1'); % optimized hyperparameters
[~, ~, fmu_opt1, ~] = gp(hyp_opt1, @infExact, [], sensor_age(1).GP_Par.covfunc, ...
    sensor_age(1).GP_Par.likfunc, Xa_opt', ya_opt1', xg_plot);
fmu_opt1( getMeasurementFromRealData(ws.vx_inter',ws, xg_plot') == 0) = NaN;

hyp_opt2 = minimize(sensor_age(1).GP_Par.hyp2, @gp, -10000, @infExact, [], ...
    sensor_age(1).GP_Par.covfunc, sensor_age(1).GP_Par.likfunc, Xa_opt', ya_opt2'); % optimized hyperparameters

[~, ~, fmu_opt2, ~] = gp(hyp_opt2, @infExact, [], sensor_age(1).GP_Par.covfunc, ...
    sensor_age(1).GP_Par.likfunc, Xa_opt', ya_opt2', xg_plot);
fmu_opt2( getMeasurementFromRealData(ws.vx_inter',ws, xg_plot') == 0) = NaN;


%% run this to save diskspace, all data are recoverable
for i=1:sPar.ns
%     sensor(i).Mo = [];
%     sensor_uni(i).Mo = [];
%     sensor_age(i).Mo = [];
%     sensor_epy(i).Mo = [];
    %
    sensor(i).Xa = []; sensor(i).ya = []; % already saved in sn.Xa and sn.ya
    sensor_uni(i).Xa = []; sensor_uni(i).ya = [];
    sensor_age(i).Xa = []; sensor_age(i).ya = [];
    sensor_epy(i).Xa = []; sensor_epy(i).ya = [];
    %
    sensor(i).Cii0 = []; sensor(i).Cis0 = []; sensor(i).Css0 = [];
    sensor(i).Cii = []; sensor(i).Cis = []; sensor(i).Css = [];
    sensor_uni(i).Cii0 = []; sensor_uni(i).Cis0 = []; sensor_uni(i).Css0 = [];
    sensor_uni(i).Cii = []; sensor_uni(i).Cis = []; sensor_uni(i).Css = [];
    sensor_age(i).Cii0 = []; sensor_age(i).Cis0 = []; sensor_age(i).Css0 = [];
    sensor_age(i).Cii = []; sensor_age(i).Cis = []; sensor_age(i).Css = [];
    sensor_epy(i).Cii0 = []; sensor_epy(i).Cis0 = []; sensor_epy(i).Css0 = [];
    sensor_epy(i).Cii = []; sensor_epy(i).Cis = []; sensor_epy(i).Css = [];
    
end
sn.Mo = []; sn_uni.Mo = []; sn_age.Mo = []; sn_epy.Mo = [];
sn.Cii0     = [];     sn.Cis0 = [];     sn.Css0 = [];
sn.Cii      = [];     sn.Cis  = [];     sn.Css  = [];
sn_uni.Cii0 = []; sn_uni.Cis0 = []; sn_uni.Css0 = [];
sn_uni.Cii  = []; sn_uni.Cis  = []; sn_uni.Css  = [];
sn_age.Cii0 = []; sn_age.Cis0 = []; sn_age.Css0 = [];
sn_age.Cii  = []; sn_age.Cis  = []; sn_age.Css  = [];
sn_epy.Cii0 = []; sn_epy.Cis0 = []; sn_epy.Css0 = [];
sn_epy.Cii  = []; sn_epy.Cis  = []; sn_epy.Css  = [];
ws.sx_s = []; ws.sy_s = []; ws.sx_i = []; ws.sy_i = [];
K = [];
L = [];
MoAGE = {}; MoEpy = {}; MoSn = {}; MoUni = {};
Sx = []; Sy = []; Xtmp = []; Ytmp = [];
GP_Par0.L = []; GP_Par0.alpha = [];

%% plot figures
% run('./FigErrOcean/plotFig.m');
% run('./FigPreMeanOcean/plotFig.m');
run('./FigErrMeanOcean/plotFig.m');
