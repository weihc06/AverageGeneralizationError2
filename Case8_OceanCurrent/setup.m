% This scripture sets up the configurations of the test case with Ocean
% Data
% Author: Hongchuan Wei

% Copyright (c) 2010-2016, Duke University 


%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

rng(0);

%% Workspace/Environment parameters

%%%%%%%%%%%%      Parameters of geometry of workspace             %%%%%%%%%
ws.type = 'ocean'; % type of workspace

%%%%%%%%%%%%%     read data  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path_name = '..\code_training_data\ocean_data\';
file_name = 'oscar-filter-monthly-574f00f0250b4.nc';

% open nc file, read only
ncid = netcdf.open([path_name,file_name],'NOWRITE');

% get cdf file information
[ndims,nvars,ngatts,unlimdimid] = netcdf.inq(ncid);

% get information about variables
for i=1:nvars
    [varname,xtype,dimids,natts] = netcdf.inqVar(ncid,i-1);
    disp(['ID: ', num2str(i-1), ', Name: ',varname]);
end

% get variable values
nc.lat = double(flipud(netcdf.getVar(ncid, 2)));
nc.lon = double(netcdf.getVar(ncid, 3) - 360);
nc.vx = double(fliplr(netcdf.getVar(ncid,4)));
nc.vy = double(fliplr(netcdf.getVar(ncid,5)));
nodatalonlat = [-125.5, 48.5; -126.5, 48.5; -124.5,47.5];
for iii = 1:size(nodatalonlat,1)
    nodatalon = nodatalonlat(iii,1);
    nodatalat = nodatalonlat(iii,2);
    nodataidx1 = find(abs(nc.lon - nodatalon)<0.1);
    nodataidx2 = find(abs(nc.lat - nodatalat)<0.1);
    nc.vx(nodataidx1,nodataidx2) = NaN;
    nc.vy(nodataidx1,nodataidx2) = NaN;
end
if 0
    % plot unprocessed data
    quiver(nc.lon,nc.lat,nc.vx',nc.vy');
end

%%
%%%%%%%%%%%%% processing data for workspace   %%%%%%%%%%%%%
ws.vx = nc.vx;
ws.vy = nc.vy;

ws.x0 = [nc.lon(1), nc.lat(1)]; % origin of workspace
ws.Lx = nc.lon(end) - nc.lon(1); % X, Y length of workspace
ws.Ly = nc.lat(end) - nc.lat(1);
ws.dx = nc.lon(2)-nc.lon(1);
ws.dy = nc.lat(2)-nc.lat(1);
ws.x_mesh = ws.x0(1): ws.dx: ws.x0(1)+ws.Lx;
ws.y_mesh = ws.x0(2): ws.dy: ws.x0(2)+ws.Ly;

% %%%%%%%%%%%%      Parameters of accessible sensing locations      %%%%%%%%%
ws.sensing_dis = 'random'; % distribution of sensing locations
ws.n_s = 100; % the number of accessible sensing locations in total
ii = 1;
ws.sx_s = zeros(1,ws.n_s);
ws.sy_s = zeros(1,ws.n_s);
ws.Xs = zeros(2,ws.n_s);
while ii <= ws.n_s
    xt = [rand*ws.Lx; rand*ws.Ly] + ws.x0';
    if getMeasurementFromRealData(ws.vx, ws, xt) % 1 is data, 0 not data
        ws.Xs(:,ii) = xt;
        ii = ii+1;
    end
end
ws.sx_s = ws.Xs(1,:); ws.sy_s = ws.Xs(2,:);

%%%%%%%%%%%%      Parameters of accessible sensing locations      %%%%%%%%%
% ws.sensing_dis = 'grid'; % distribution of points of interest
% sx_s = ws.x0(1)+0.1: ws.dx*1: ws.x0(1)+ws.Lx;
% sy_s = ws.x0(2)+0.1: ws.dy*1: ws.x0(2)+ws.Ly;
% [X_mesh, Y_mesh] = meshgrid( sx_s, sy_s);
% X_mesh_v = X_mesh(:)';
% Y_mesh_v = Y_mesh(:)';
% tmp_if_data = getMeasurementFromRealData(ws.vx, ws, [X_mesh_v; Y_mesh_v]);
% idx_if_data = find(tmp_if_data);
% ws.sx_s = X_mesh_v(idx_if_data);
% ws.sy_s = Y_mesh_v(idx_if_data);
% ws.Xs = [ws.sx_s; ws.sy_s]; % 2xn points of interest, for the laziness
% ws.n_s = length(ws.sx_s);

if 0
    % plot unprocessed data
    hold on
    quiver(nc.lon,nc.lat,nc.vx',nc.vy');
    scatter(ws.Xs(1,:), ws.Xs(2,:),'.');
end

%%%%%%%%%%%%      Parameters of test points/points of interest   %%%%%%%%%%
ws.poi_dis = 'grid'; % distribution of points of interest
sx_i = ws.x0(1)+0.1: ws.dx*10: ws.x0(1)+ws.Lx;
sy_i = ws.x0(2)+0.1: ws.dy*10: ws.x0(2)+ws.Ly;
[X_mesh, Y_mesh] = meshgrid( sx_i, sy_i);
X_mesh_v = X_mesh(:)';
Y_mesh_v = Y_mesh(:)';
tmp_if_data = getMeasurementFromRealData(ws.vx, ws, [X_mesh_v; Y_mesh_v]);
idx_if_data = find(tmp_if_data);
ws.sx_i = X_mesh_v(idx_if_data);
ws.sy_i = Y_mesh_v(idx_if_data);
ws.Xi = [ws.sx_i; ws.sy_i]; % 2xn points of interest, for the laziness



%%%%%%%%%%%%      parameters of simulation time                %%%%%%%%%%%%
ws.tf = 200; % simulation total time, (ws.dt = 1, implied)

%% Gaussian Process Parameters for Latent Variables/Function/GP process

% latent function related 
GP_Par0.covfunc = @covSEiso; % squared exponential cov
% GP_Par0.hyp1.cov = [log(sqrt(10)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
% GP_Par0.hyp2.cov = [log(sqrt(10)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
GP_Par0.hyp1.cov = [log(sqrt(100)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
GP_Par0.hyp2.cov = [log(sqrt(1000)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
% measurement/observation/training data related
GP_Par0.likfunc = @likGauss; % Gaussian likelihood function/measurement func
GP_Par0.sigma_n = sqrt(0.1); % standard deviation of measurement noise
GP_Par0.hyp1.lik = log(GP_Par0.sigma_n); % log(sigma_n)
GP_Par0.hyp2.lik = log(GP_Par0.sigma_n); % log(sigma_n)

%% mesh grids
ng_plot = floor( [60, 40] );
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

% % % %% regression result with hyper-parameter optimization
ng_opt = floor( [60, 40] );
[X1_opt, X2_opt] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_opt(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_opt(2)));
Xa_opt_tmp = [X1_opt(:), X2_opt(:)];
Xa_opt = Xa_opt_tmp( getMeasurementFromRealData(ws.vx, ws, Xa_opt_tmp') == 1, :)';
ya_opt1 = getMeasurementFromRealData(ws.vx, ws, Xa_opt, GP_Par0.sigma_n);
ya_opt2 = getMeasurementFromRealData(ws.vy, ws, Xa_opt, GP_Par0.sigma_n);
% % % 
% % % hyp_opt1 = minimize(GP_Par0.hyp1, @gp, -10000, @infExact, [], ...
% % %     GP_Par0.covfunc, GP_Par0.likfunc, Xa_opt', ya_opt1'); % optimized hyperparameters
% % % [~, ~, fmu_opt1, ~] = gp(hyp_opt1, @infExact, [], GP_Par0.covfunc, ...
% % %     GP_Par0.likfunc, Xa_opt', ya_opt1', xg_plot);
% % % fmu_opt1( getMeasurementFromRealData(ws.vx,ws, xg_plot') == 0) = NaN;
% % % 
% % % hyp_opt2 = minimize(GP_Par0.hyp2, @gp, -10000, @infExact, [], ...
% % %     GP_Par0.covfunc, GP_Par0.likfunc, Xa_opt', ya_opt2'); % optimized hyperparameters
% % % 
% % % [~, ~, fmu_opt2, ~] = gp(hyp_opt2, @infExact, [], GP_Par0.covfunc, ...
% % %     GP_Par0.likfunc, Xa_opt', ya_opt2', xg_plot);
% % % fmu_opt2( getMeasurementFromRealData(ws.vx,ws, xg_plot') == 0) = NaN;
% % % 
% % % ws.vx =  reshape(fmu_opt1,ng_plot(2),ng_plot(1));
% % % ws.vy =  reshape(fmu_opt2,ng_plot(2),ng_plot(1));

[~, ~, fmu_inter1, ~] = gp(GP_Par0.hyp1, @infExact, [], GP_Par0.covfunc, ...
    GP_Par0.likfunc, Xa_opt', ya_opt1', xg_plot);
fmu_inter1( getMeasurementFromRealData(ws.vx,ws, xg_plot') == 0) = NaN;
[~, ~, fmu_inter2, ~] = gp(GP_Par0.hyp2, @infExact, [], GP_Par0.covfunc, ...
    GP_Par0.likfunc, Xa_opt', ya_opt2', xg_plot);
fmu_inter2( getMeasurementFromRealData(ws.vx,ws, xg_plot') == 0) = NaN;

ws.vx_inter =  reshape(fmu_inter1,ng_plot(2),ng_plot(1));
ws.vy_inter =  reshape(fmu_inter2,ng_plot(2),ng_plot(1));
nodatalonlat = [-133.1, 55.39; -129, 53.34; -155.4,57.45];
for iii = 1:size(nodatalonlat,1)
    nodatalon = nodatalonlat(iii,1);
    nodatalat = nodatalonlat(iii,2);
    nodataidx1 = find(abs(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)) - nodatalon)<0.1);
    nodataidx2 = find(abs(linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)) - nodatalat)<0.1);
    ws.vx_inter(nodataidx2,nodataidx1) = NaN;
    ws.vy_inter(nodataidx2,nodataidx1) = NaN;
end
%% plot map using surf
flag_plot_map_surf = 0
if flag_plot_map_surf
    
    [fp.img, map] = imread([path_name,'FigOceanMap2.png']);
    fp.img = flipud(fp.img);
    [xl,yl] = size(fp.img);
%     fp.x = [-184 -96]-0.5;
%     fp.y(1) = 17.5/24.5*20;
%     fp.y(2) = 63;
    fp.x = [-188 -96]+3;
    fp.y(1) = 17.5/24.5*20+1;
    fp.y(2) = 65+1;

    figure
    hold on
    
    image(fp.x, fp.y, fp.img);
    
    if 0
        [x_lim, y_lim] = ginput;
    else
        load('./xlim.mat','x_lim','y_lim');
    end
    
    fill(x_lim,y_lim,'r')
%     quiver(nc.lon,nc.lat,nc.vx',nc.vy');
%     quiver(X1_plot,X2_plot,ws.vx,ws.vy);
    quiver(X1_plot,X2_plot,ws.vx_inter, ws.vy_inter);
    % plot sensing locations
    scatter3(ws.Xs(1,:), ws.Xs(2,:), 100*ones(1,size( ws.Xs,2) ), 200, rgb('Red'),'.')
    
    % plot points of interests
    scatter3(ws.Xi(1,:), ws.Xi(2,:), 100*ones(1,size( ws.Xi,2) ), 25, rgb('Black'),'d','filled')
    

%     legend({'Spatial Phenomenon','Sensing location','Points of interest'},'Location','southoutside')
    axis equal
    axis tight
    set(gca,'Xlim',[-160 -100],'YLim',[20, 60]);
    grid on
    box on
    set(gcf,'Color','w')
    if 0
%         export_fig ./FigMapSurf.png -r300
%         export_fig ./FigMapSurf.pdf -r300
%         savefig(gcf,'FigMapSurf.fig');
    end
    hold off
end




%% Sensor paparameters
sPar.ns = 1; % number of sensors
sPar.sigma_n = GP_Par0.sigma_n; % measurement noise standard variance

%% Sensor Network Structs for Theoretical Curves, Uniform Random
sn.GP_Par = GP_Par0;
%%%%%%%%%%%%      Parameters related to measurements/training data %%%%%%%%
%%%%%%%%%%%%      of the entire sensor network                     %%%%%%%%

% Mo: initial observation vector/mask of sensing locations
%   0: no measurement(can not be measured); 
%   1: measurement; 
%   2: not measured (yet).
sn.Mo = 2*ones(1, ws.n_s ); % assume that all positions can be measured

%..........................................................................
% ....    test if the algorithm works when there are measurement     .... %
% ....    that are already obtained at the initial time              .... %
% nO = 0; % floor(ws.ns/3); % running test of the algorithm
% random_tmp = unidrnd( ws.nx_s, 2, nO);
% for jj = 1:size(random_tmp,2)
%     sn.Mo(random_tmp(1,jj), random_tmp(2,jj)) = 1;
% end
%..........................................................................

% matrix of observed measurement positions, d x n matrix, where d is the
% dimension of input, n is the number of observations
sn.Xa0 = [ws.sx_s(find(sn.Mo == 1));...
          ws.sy_s(find(sn.Mo == 1))]; 
sn.Xa = sn.Xa0;
% get noisy measurements from an underlying GP process determined by
% GP_Par0
% ws.ya is a vector of 1 x n matrix/vector, containing all the noisy
% measurements
sn.ya0 = getMeasurementFromRealData(ws.vx, ws, sn.Xa0, sPar.sigma_n);
sn.ya = sn.ya0;


%%%%%%%%%%%%      get initial covariance matrices              %%%%%%%%%%%%
% [varargout] = gp(hyp, inf, mean, cov, lik, x, y, xs, ys)
if ~isempty(sn.Xa0) 
    [~, ~, ~, fs2g] = gp(GP_Par0.hyp, @infExact, [], GP_Par0.covfunc, GP_Par0.likfunc, sn.Xa0', sn.ya0, ws.Xi');
    sn.Cii0 = fs2g;
    [~, ~, ~, fs2g] = gp(GP_Par0.hyp, @infExact, [], GP_Par0.covfunc, GP_Par0.likfunc, sn.Xa0', sn.ya0, ws.Xs');
    sn.Css0 = fs2g;
    
    Caa = feval(GP_Par0.covfunc, GP_Par0.hyp.cov, sn.Xa0', sn.Xa0') + eye(size(sn.Xa0,2))*(GP_Par0.sigma_n^2);
    Cis = feval(GP_Par0.covfunc, GP_Par0.hyp.cov, ws.Xi', ws.Xs');
    Cia = feval(GP_Par0.covfunc, GP_Par0.hyp.cov, ws.Xi', sn.Xa0');
    Cas = feval(GP_Par0.covfunc, GP_Par0.hyp.cov, sn.Xa0', ws.Xs');    
    sn.Cis0 = Cis - Cia /Caa *Cas;
else
    % initial covariance matrices
    sn.Cii0 = feval(GP_Par0.covfunc, GP_Par0.hyp1.cov, ws.Xi'); % x: n by D matrix of inputs
    sn.Cis0 = feval(GP_Par0.covfunc, GP_Par0.hyp1.cov, ws.Xi', ws.Xs'); % x: n by D matrix of inputs
    sn.Css0 = feval(GP_Par0.covfunc, GP_Par0.hyp1.cov, ws.Xs');
end
% matrices without '0' are subject to change
sn.Cii = sn.Cii0;
sn.Cis = sn.Cis0;
sn.Css = sn.Css0;

sn.err_his = {}; % history of generalization errors
                 % assumed to be cell array since two values can occur at
                 % the time of cummunication
%% Individual Sensors for Theoretical Results, Uniform Random Algorithm
for i=1:sPar.ns
    sensor(i) = sn;
end
for i=1:sPar.ns % additional features in subworkspace
    %   Mo: matrix stores which positions have been observed
    %       0: observed, but could not obtain measurement
    %       1: observed, and obtained measurement
    %       2: positions not checked yet
    sensor(i).Mo = 2*ones(1,ws.n_s); % mask of sensing locations
    sensor(i).Xa0 = [];
    sensor(i).ya0 = [];
    sensor(i).Xa = sensor(i).Xa0; % 2xn matrix of inputs before last communication, all sensors'
    sensor(i).ya = sensor(i).ya0; % vector of outputs before last communication 
    sensor(i).Xl = sensor(i).Xa0; % 2xn matrix of inputs, local sensor's
    sensor(i).yl = sensor(i).ya0; 
    sensor(i).err_his = {}; % history of generalization errors
    sensor(i).X_new = [];  % vector of new observed positions/input 
    sensor(i).y_new1 = [];   % row vector of new measurements/observations/output data
    sensor(i).y_new2 = [];
end
% the index of the first measurement in Mo, in order to make the initial
% measurement position the same
sensor(1).idx1 = 12;


%% Individual Sensors for Simulation Results
sensor_uni = sensor; % simulation result for Uniformly Random 
sensor_age = sensor; % simulation result for Greedy Algorithm that maximizes AGE
sensor_epy = sensor; % simulation result for Greedy Algorithm that maximizes Entropy


