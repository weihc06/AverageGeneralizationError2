%%
% Functionality: This is the main function of the
% AVERAGE_GENERALIZATION_ERROR_2 project. 
% Matlab version tested: R2015b
% Author: Hongchuan Wei
% Date: 4/11/16

clear
close all
% clc

%% add necessary libraries
addpath('./code_communicate/');
addpath('./code_policy/');
addpath('./code_training_data/');
addpath('./export_fig/');
addpath(genpath('./gpml-matlab-v3.6-2015-07-07/'));

%% specify test case directory
% test_case_dir = './Case1_AGE_OLD';
% test_case_dir = './Case2_AGE_OLD';
% test_case_dir = './Case3_WithMeasurement';
% test_case_dir = './Case3a_WithMeasureDiffCom';
test_case_dir = './Case3b_WithMeaureHyperOpt';
% test_case_dir = './Case4_MapData';
% test_case_dir = './Case5_MapDataCov';

% addpath(test_case_dir);

%% call main function 
run([test_case_dir,'/main.m']);

