function [err, Cii, Cis, Css] = getAGENext(Cii, Cis, Css, Mo, sigma_n, sigma_f)
% Functionality:
%   This function calculated the expected averagle generalization error for
%   the next time step, given the current covariance matrices Cii, Cis,
%   Css, and the distribution of the next sensing location based on the
%   matrix Mo.
% Input
%   Mo: nx x ny matrix stores which positions have been observed
%       0: observed, but could not obtain measurement
%       1: observed, and obtained measurement
%       2: positions not checked yet
%       NOTICE: ns = nx * ny, Ni jiu shi pi ma.
%   Cii: ni by ni covariance matrix of points of interests
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations
%   sigma_n: standard deviation of measurement noise
%   sigma_f: coeffient multiplied to covariance function
% Output
%   err: normalized approximated expected average generalization error (AEAGE)
%   Cii: ni by ni covariance matrix of points of interests
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations
% Copyright (c) 2010-2016, Duke University 


    % get the number of undiscoverd sensing locations, assuming uniform
    % random distribution over undiscovered sensing locations. 

    tmp = (Mo == 2);
    nUnchecked = sum( sum( tmp ) );
    Ps = 1/nUnchecked;

    invD = 1./(diag(Css) + sigma_n^2);
    invD(Mo==0) = 0;

    % Update Cii, Cis, and Css based on Mo
    invD = diag(invD);
    Cii = Cii - Cis* sparse(invD) * Cis' .* Ps;
    Cis = Cis - Cis* sparse(invD) * Css .* Ps;
    Css = Css - Css* sparse(invD) * Css .* Ps;

    % get error
    err = trace(Cii)/size(Cii,1)/sigma_f^2;
% err = trace(Cii)/size(Cii,1);
end