% close all

%% add necessary libraries
addpath('../');
addpath('../../');
addpath('../../code_communicate/');
addpath('../../code_policy/');
addpath('../../code_training_data/');
addpath(genpath('../../gpml-matlab-v3.6-2015-07-07/'));
addpath('../../export_fig/');

flag_save_fig = 0;
flag_title = 0;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

sigma_n = GP_Par.sigma_n;

%% plot map and POI and sensing positions using geo

    figure
    hold on
    
    R = georasterref('RasterSize',[ng_plot(2), ng_plot(1)],...
        'Latlim', [data_hdr.ULYMAP-data_hdr.NROWS*data_hdr.YDIM, data_hdr.ULYMAP],...
        'Lonlim', [data_hdr.ULXMAP, data_hdr.ULXMAP+data_hdr.NCOLS*data_hdr.XDIM]);
    Z = reshape(fmu_opt, ng_plot(2), ng_plot(1));
    hg = worldmap(Z,R);
    hgf = geoshow(Z,R,'DisplayType','surface', 'ZData',zeros(size(Z)),'CData',Z);
    hg.Children(12).LineStyle = ':';
    hg.Children(12).LineWidth = 0.5;
    for i=1:8
        hg.Children(i).FontSize = 14;
    end

    
%     text('Clipping','on','FontSize',15,'Interpreter','latex',...
%     'String','$\hat{f}(\mathbf{x})$',...
%     'Position',[2628212.21002673 5654742.13883013 0],...
%     'Visible','on');

% Create text (c)
text('Clipping','on','FontSize',15,'Interpreter','latex',...
    'String','$(a)$',...
    'Position',[-2816608.49786174 5682171.96607389 0],...
    'Visible','on');
    
    % figure properties
%     c = colorbar('Position', [0.8696 0.279 0.048 0.439]);
 hc = colorbar;
    hc.Units = 'pixels';
    hc.FontSize = 15;
    hc.Position = [484 118.384322870868 19.9999996182028 207.615677129132];
    
    
    
    set(gca,'FontSize',15);
    set(gcf,'Color','w');
    if 1
        saveas(gcf,'./FigPredictMeanFcnHyperOptMap.fig');
        export_fig ./FigPredictMeanFcnHyperOptMap.png -r500
    end
    hold off













