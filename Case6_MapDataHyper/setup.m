% This scripture sets up the configurations of the test case
% Author: Hongchuan Wei

% Copyright (c) 2010-2016, Duke University 

%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

rng(0);

%% Workspace/Environment parameters

%%%%%%%%%%%%      Parameters of geometry of workspace             %%%%%%%%%
ws.type = 'map'; % type of workspace

%%%%%%%%%%%%%     read data  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path_name = '..\code_training_data\PRISM_tmean_provisional_4kmM2_201603_bil\';
file_name = 'PRISM_tmean_provisional_4kmM2_201603_bil';

% read header file
path_file_name_hdr = [path_name, file_name, '.hdr'];
data_file_hdr = fopen(path_file_name_hdr);
header = textscan(data_file_hdr,'%s',28);
data_hdr.BYTEORDER = header{1}{2};
data_hdr.LAYOUT = header{1}{4};
data_hdr.NROWS  = str2num(header{1}{6});
data_hdr.NCOLS  = str2num(header{1}{8});
data_hdr.NBANDS = str2num(header{1}{10});
data_hdr.NBITS  = header{1}{12};
data_hdr.PIXELTYPE = lower(header{1}{18});
data_hdr.ULXMAP = str2num(header{1}{20});
data_hdr.ULYMAP = str2num(header{1}{22});
data_hdr.XDIM   = str2num(header{1}{24});
data_hdr.YDIM   = str2num(header{1}{26});
data_hdr.NODATA = str2num(header{1}{28});
% compose parameters for multibandread function
data_hdr.offset = 0; % since the header is not included in this file
data_hdr.size = [ data_hdr.NROWS, data_hdr.NCOLS, data_hdr.NBANDS]; % [ NROWS NCOLS NBANDS]
data_hdr.interleave = data_hdr.LAYOUT;
data_hdr.precision = [data_hdr.PIXELTYPE, data_hdr.NBITS];
if strcmp(data_hdr.BYTEORDER, 'I')
    data_hdr.byteorder = 'ieee-le';              % BYTEORDER = I (refers to Intel order or little endian format)
else
    error('data_hdr.BYTEORDER type error');
end

%%%%%%%%%%%%%     read data from data file  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path_file_name_bil = [path_name, file_name, '.bil'];
% temperature unit: Celsius degree
data_file_bil = multibandread(path_file_name_bil, ...
    data_hdr.size, data_hdr.precision, data_hdr.offset, data_hdr.interleave, data_hdr.byteorder);

data_file_bil_plot = flipud(data_file_bil);
data_file_bil_plot( data_file_bil_plot == data_hdr.NODATA ) = NaN;
% NOTICE that dim1 of data_file_bil_plot is latitude data (Y direction)
%             dim2 of data_file_bil_plot is longitude data (X direction)

%%%%%%%%%%%%% processing data for workspace   %%%%%%%%%%%%%
ws.data_file_bil_plot = data_file_bil_plot;
% clearvars data_file_bil_plot data_file_bil
data_hdr.ULYMAP2 = data_hdr.ULYMAP - data_hdr.YDIM * (data_hdr.NROWS-1);
ws.x0 = [data_hdr.ULXMAP, data_hdr.ULYMAP2]; % origin of workspace
ws.Lx = (data_hdr.NCOLS-1)*data_hdr.XDIM; % X, Y length of workspace
ws.Ly = data_hdr.YDIM * (data_hdr.NROWS-1);
ws.dx = data_hdr.XDIM;
ws.dy = data_hdr.YDIM;
ws.x_mesh = ws.x0(1): ws.dx: ws.x0(1)+ws.Lx;
ws.y_mesh = ws.x0(2): ws.dy: ws.x0(2)+ws.Ly;


%% Gaussian Process Parameters for Latent Variables/Function/GP process

% .... covariance function/Latent function related ....
% 1. squared exponential cov
% GP_Par.covfunc = {@covSEiso}; % squared exponential cov
% GP_Par.hyp.cov = [log(sqrt(1)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
GP_Par.covfunc = {@covSEard};
GP_Par.hyp.cov = [log(sqrt(10000)); log(1); 0];

% 2a. Squared Exponential covariance function with spatially varying lengthscale.
%    The hyperparameters of covSEvlen are: hyp = [ hyp_len; log(sf) ]^T
%    where len is the spatially varying lengthscale (here specified in the log
%    domain), D is the dimension of the input data and sf^2 is the signal variance.
%    The log-lengthscale function llen is supposed to be a valid GPML mean function
%    with hyperparameters hyp_len.
%    Spatially varying lenthscale determined by Linear mean function. 
%    The mean function is parameterized as: m(x) = sum_i a_i * x_i;
%    The hyperparameter is: hyp = [ a_1 a_2 ... a_D ]^T

% GP_Par.covfunc = {@covSEvlen,{@meanLinear}};
% GP_Par.hyp.cov = [0.01;0.01; 0]; % hyp = [ hyp_len; log(sf) ]^T

% 2b. Squared Exponential covariance function with spatially varying lengthscale.
%    Spatially varying lenthscale determined by Constant mean function. 
%    The hyperparameter is: hyp = [ c ]^T
% GP_Par.covfunc = {@covSEvlen,{@meanConst}};
% GP_Par.hyp.cov = [log(sqrt(100)); 0]; % hyp = [ hyp_len; log(sf) ]^T

% 2c. Squared Exponential covariance function with spatially varying lengthscale.
% mc = {@meanConst}; hypc = 45;
% ml = {@meanLinear}; hypl = [0.2 0]';
% msu = {@meanSum,{mc,ml}}; 
% hypsu = [hypc; hypl];
% GP_Par.covfunc = {@covSEvlen, {msu{:}} };
% GP_Par.hyp.cov = [hypsu; 0]; % hyp = [ hyp_len; log(sf) ]^T

% 4. Rational Quadratic covariance function with Automatic Relevance Determination
%    (ARD) distance measure. The covariance function is parameterized as:
%    k(x^p,x^q) = sf^2 * [1 + (x^p - x^q)'*inv(P)*(x^p - x^q)/(2*alpha)]^(-alpha)
%    where the P matrix is diagonal with ARD parameters ell_1^2,...,ell_D^2, where
%    D is the dimension of the input space, sf2 is the signal variance and alpha
%    is the shape parameter for the RQ covariance. The hyperparameters are:
%    hyp = [ log(ell_1) log(ell_2) ... log(ell_D) log(sf) log(alpha) ]'
% GP_Par.covfunc = {'covRQard'}; 
% GP_Par.hyp.cov = log([1 1 1 1]');

% 5. Neural network covariance function with a single parameter for the distance
%    measure. The covariance function is parameterized as:
%    k(x^p,x^q) = sf2 * asin(x^p'*P*x^q / sqrt[(1+x^p'*P*x^p)*(1+x^q'*P*x^q)])
%    where the x^p and x^q vectors on the right hand side have an added extra bias
%    entry with unit value. P is ell^-2 times the unit matrix and sf2 controls the
%    signal variance. The hyperparameters are: hyp = [ log(ell) log(sqrt(sf2) ]
% GP_Par.covfunc = {@covNNone}; % squared exponential cov
% GP_Par.hyp.cov = [log(sqrt(100)); 0]; % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T

% 6. Matern covariance function with nu = d/2 and with Automatic Relevance
%    Determination (ARD) distance measure. For d=1 the function is also known as
%    the exponential covariance function or the Ornstein-Uhlenbeck covariance 
%    in 1d. The covariance function is:
%    k(x^p,x^q) = sf^2 * f( sqrt(d)*r ) * exp(-sqrt(d)*r)
%    with f(t)=1 for d=1, f(t)=1+t for d=3 and f(t)=1+t+t²/3 for d=5.
%    Here r is the distance sqrt((x^p-x^q)'*inv(P)*(x^p-x^q)), where the P matrix
%    is diagonal with ARD parameters ell_1^2,...,ell_D^2, where D is the dimension
%    of the input space and sf2 is the signal variance. The hyperparameters are:
%    hyp = [ log(ell_1)  log(ell_2) .. log(ell_D) log(sf) ]'
% GP_Par.covfunc = {@covMaternard ,5}; % squared exponential cov
% GP_Par.hyp.cov = [log(10); log(10); 0]; % hyperparameters

% .... mean function related ....
% GP_Par.meanfunc = {@meanSum, {@meanLinear, @meanConst}}; 
% GP_Par.hyp.mean = [0; -1; 50];
% GP_Par.meanfunc = []; 
% GP_Par.hyp.mean = [];
GP_Par.meanfunc = {@meanSum, {@meanLinear, @meanConst}}; 
GP_Par.hyp.mean = [0; 0; 0];

% .... likelihood/measurement/observation/training data related ....
GP_Par.likfunc = @likGauss; % Gaussian likelihood function/measurement func
GP_Par.sigma_n = sqrt(0.1); % standard deviation of measurement noise
GP_Par.hyp.lik = log(GP_Par.sigma_n); % log(sigma_n)





