% This test case tests a new covariance function, maybe not isotropic 
% with GPML package
% Copyright (c) 2010-2016, Duke University 

%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

%% set up configurations
run('./setup.m');

%% mesh grids
ng_plot = [140, 60]*4;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot(2)));
xg_plot = [X1_plot(:), X2_plot(:)];

%% true function 
[~, fmu_plot2] = getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot', GP_Par.sigma_n);

%% regression result without hyper-parameter optimization
ng_hyper0 = floor( [140, 60]/3 );
[X1_hyper0, X2_hyper0] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_hyper0(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_hyper0(2)));
xg_hyper0 = [X1_hyper0(:), X2_hyper0(:)];

Xa_hyper0 = xg_hyper0( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_hyper0') == 1, :)';
ya_hyper0 = getMeasurementFromRealData(ws.data_file_bil_plot',ws, Xa_hyper0, GP_Par.sigma_n);

[~, ~, fmu_hyper0, ~] = gp(GP_Par.hyp, @infExact, GP_Par.meanfunc, GP_Par.covfunc, GP_Par.likfunc, Xa_hyper0', ya_hyper0', xg_plot);
fmu_hyper0( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot') == 0) = NaN;

%% regression result with hyper-parameter optimization
ng_opt = floor( [140, 60] );
[X1_opt, X2_opt] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_opt(1)),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_opt(2)));
Xa_opt_tmp = [X1_hyper0(:), X2_hyper0(:)];
Xa_opt = Xa_opt_tmp( getMeasurementFromRealData(ws.data_file_bil_plot',ws, Xa_opt_tmp') == 1, :)';
ya_opt = getMeasurementFromRealData(ws.data_file_bil_plot',ws, Xa_opt, GP_Par.sigma_n);


hyp_opt = minimize(GP_Par.hyp, @gp, -10000, @infExact, GP_Par.meanfunc, ...
    GP_Par.covfunc, GP_Par.likfunc, Xa_opt', ya_opt'); % optimized hyperparameters
[~, ~, fmu_opt, ~] = gp(hyp_opt, @infExact, GP_Par.meanfunc, GP_Par.covfunc, GP_Par.likfunc, Xa_opt', ya_opt', xg_plot);
fmu_opt( getMeasurementFromRealData(ws.data_file_bil_plot',ws, xg_plot') == 0) = NaN;

%% plot figure

clear;
close all;
load('r1.mat');

run('FigPredictMeanFcnMapV2/plotFig.m');

% run('FigPredictMeanFcnHyperOptMapV1/plotFig.m');
% 
% run('FigPredictErrMapV1\plotFig.m');
% 
% run('FigPredictErrHyperOptMapV1\plotFig.m');


%%
% % run('FigTrueFcnMapV1/plotFig.m');
% % 
% % run('FigPredictMeanFcnMapV1/plotFig.m');
% % 
% % run('FigPredictMeanFcnHyperOptMapV1/plotFig.m');
% % 
% % run('FigPredictErrMapV1\plotFig.m');
% % 
% % run('FigPredictErrHyperOptMapV1\plotFig.m');


