%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

flag_save_fig = 1;

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

clr_seq_ws = [ rgb('Green'); rgb('Gold'); rgb('Tomato'); rgb('Purple')];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');







%% plot regression result
figure
hold on
% process all measurement inputs and outputs
X_new_plot = [];
y_new_plot = [];

sensor_plot = sensor_age;   
sn_plot = sn_age;

for i=1:length(sensor_plot)
    X_new_plot = [X_new_plot, sensor_plot(i).X_new];
    y_new_plot = [y_new_plot, sensor_plot(i).y_new];
end

Xa_plot = [sn_plot.Xa, X_new_plot];
ya_plot = [sn_plot.ya, y_new_plot];
    

ng_plot = 20;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot, fs2_plot] = gp(GP_Par.hyp, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);

% plot the figure

surf(X1_plot, X2_plot, reshape(fmu_plot, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
hs = scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 30, rgb('Red'),'o','filled');
text(0, 95, 40,'(b)','Interpreter','latex','FontSize',15)

xlabel('','Interpreter','latex');
text('String','$x$ (m)', 'Interpreter','latex','FontSize',15,'Position',[176.924677963456 15.8080712785552 -71.1305515045526]);
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$\hat{f}(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
legend({'$\hat{f}(\mathbf{x})$','$y_i$'},'Interpreter','latex','FontSize',15,...
    'Position',[0.738996165062814 0.728174604877593 0.127908596841948 0.06984126813828]); 
% 'Position',[0.6,0.65,0.15,0.15],

zlim([-40 40])
view(-79,12) 
if flag_save_fig
    saveas(gcf,'./FigPredictMeanFcn.fig')
    export_fig('./FigPredictMeanFcn.png');
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcn','-dpdf','-r0')
end
hold off

%% optimize hyper-parameter

hyp_opt = minimize(GP_Par.hyp, @gp, -100, @infExact, [], ...
    GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot'); % optimized hyperparameters
[~, ~, fmu_opt, fs2_opt] = gp(hyp_opt, @infExact, [], GP_Par.covfunc, GP_Par.likfunc, Xa_plot', ya_plot', xg_plot);

% plot the figure
figure
hold on
surf(X1_plot, X2_plot, reshape(fmu_opt, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 30, rgb('Red'),'o','filled');
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
text(0, 95, 40,'(c)','Interpreter','latex','FontSize',15)
% figure properties
xlabel('','Interpreter','latex');
text('String','$x$ (m)', 'Interpreter','latex','FontSize',15,'Position',[176.924677963456 15.8080712785552 -71.1305515045526]);
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$\hat{f}(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
legend({'$\hat{f}(\mathbf{x})$','$y_i$'},'Interpreter','latex','FontSize',15,...
    'Position',[0.738996165062814 0.728174604877593 0.127908596841948 0.06984126813828]); 
% 'Position',[0.6,0.65,0.15,0.15],
% axis tight
view(-79,12)
zlim([-40 40]);
if flag_save_fig
    saveas(gcf,'./FigPredictMeanFcnOptHyper.fig')
    export_fig ./FigPredictMeanFcnOptHyper.png -r300;
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigPredictMeanFcnOptHyper','-dpdf','-r300')
end
hold off


%% plot the original surface

ng_plot = 20;
[X1_plot, X2_plot] = meshgrid(linspace(ws.x0(1),ws.x0(1)+ws.Lx,ng_plot),linspace(ws.x0(2),ws.x0(2)+ws.Ly,ng_plot));
xg_plot = [X1_plot(:), X2_plot(:)];

[~, ~, fmu_plot2, fs2_plot2] = gp(GP_Par0.hyp, @infExact, [], GP_Par0.covfunc, ...
    GP_Par0.likfunc, GP_Par0.Xr', GP_Par0.yr', xg_plot);

% plot the figure
figure
hold on
surf(X1_plot, X2_plot, reshape(fmu_plot2, ng_plot, ng_plot), 'FaceAlpha',0.5, 'LineStyle','none','FaceColor','interp'); % plot the learned mean function
% scatter3(Xa_plot(1,:), Xa_plot(2,:), ya_plot, 200, rgb('Red'),'.')
text(0, 95, 40,'(a)','Interpreter','latex','FontSize',15)
% figure properties
xlabel('','Interpreter','latex');
text('String','$x$ (m)', 'Interpreter','latex','FontSize',15,'Position',[176.924677963456 15.8080712785552 -71.1305515045526]);
text('String','$f(\mathbf{x}) = xy$','Interpreter','latex','FontSize',15,...
    'Position',[190.760383392781 51.9511677098385 5.94232664349045]);
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$f(\mathbf{x})$', 'Interpreter','latex');
% title({'Predicted Mean Function'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
% legend({ 'Ground-truth Fun', 'Training Data'},'Position',[0.6,0.65,0.3,0.15],'Interpreter','latex'); 
% legend({ 'Ground-truth Fun'},'Position',[0.6,0.70,0.3,0.075],'Interpreter','latex'); 

% axis equal
view(-79,12) 
if flag_save_fig
    saveas(gcf,'./FigTrueFcn.fig')
    export_fig('./FigTrueFcn.png');
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(gcf,'./FigTrueFcn','-dpdf','-r0')
end
hold off



%% plot error between predicted mean function and ground truth function
figure
hold on
surf(X1_plot, X2_plot, reshape(abs(fmu_plot2-fmu_plot), ng_plot, ng_plot), 'FaceAlpha',1, 'LineStyle','none','FaceColor','interp');
text(0, 95, 20,'(d)','Interpreter','latex','FontSize',15)

% figure properties
xlabel('','Interpreter','latex');
text('String','$x$ (m)', 'Interpreter','latex','FontSize',15,'Position',[176.636011304265 16.7215809649384 -7.88525260911505]);
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$|\hat{f}(\mathbf{x})-f(\mathbf{x})|$', 'Interpreter','latex');
% title({'Prediction Error'}, 'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
% axis equal
view(-79,12) 
if flag_save_fig
    saveas(gcf,'./FigPredictError.fig')
    export_fig ./FigPredictError.png -r300;
    export_fig ./FigPredictError.pdf;
end
hold off


%% plot error between predicted mean fun with optimized hyper-par and ground truth fun
figure
hold on
surf(X1_plot, X2_plot, reshape(abs(fmu_plot2-fmu_opt), ng_plot, ng_plot), 'FaceAlpha',1, 'LineStyle','none','FaceColor','interp');
text(0, 95, 20,'(e)','Interpreter','latex','FontSize',15)

% figure properties
xlabel('','Interpreter','latex');
text('String','$x$ (m)', 'Interpreter','latex','FontSize',15,'Position',[176.636011304265 16.7215809649384 -7.88525260911505]);
% text('String','$x$ (m)', 'Interpreter','latex','FontSize',15)
ylabel('$y$ (m)', 'Interpreter','latex');
zlabel('$|\hat{f}(\mathbf{x})-f(\mathbf{x})|$', 'Interpreter','latex');
% title({'Prediction Error with Optimized Hyper-parameter'}, 'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
zlim([0 20]);
az = -79; el = 12;
view(az,el) 
if flag_save_fig
    saveas(gcf,'./FigPredictErrorOptHyper.fig')
    export_fig ./FigPredictErrorOptHyper.png -r300;
    export_fig ./FigPredictErrorOptHyper.pdf;
end
hold off










