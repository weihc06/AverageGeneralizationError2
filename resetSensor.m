function sensor = resetSensor(sensor)
% Functionality:
%   This function resets the sensor (network) to its initial conditions
% Input
%   sensor: 1xns struct with fields:
%   Cii: ni by ni covariance matrix of points of interests
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations
%   Cii0: initial value of Cii
%   Cis0: initial value of Cis
%   Css0: initial value of Css
%   Xa: d x n matrix of observed measurement positions, 
%       where d is the dimension of input, n is the number of observations
%   ya: 1 x n matrix/vector, containing all the noisy measurements
%   tc: latest communication time 
%   err_his = []; % history of generalization errors 
% Output
%   sensor with fileds reset to initial values
% Copyright (c) 2010-2016, Duke University 

    for i=1:length(sensor)
        sensor(i).Cii = sensor(i).Cii0;
        sensor(i).Cis = sensor(i).Cis0;
        sensor(i).Css = sensor(i).Css0;
        sensor(i).Xa = sensor(i).Xa0;
        sensor(i).ya = sensor(i).ya0;
        sensor(i).tc = 0;
        sensor(i).err_his = {};
        if isfield(sensor, 'X_new')
            sensor(i).X_new = [];
            sensor(i).y_new = [];
        end
        if isfield(sensor, 'Xl')
            sensor(i).Xl = sensor(i).Xa0;
            sensor(i).yl = sensor(i).ya0;
        end
        if isfield(sensor, 'tc_his')
            sensor(i).tc_his = [];
        end
    end
    
end