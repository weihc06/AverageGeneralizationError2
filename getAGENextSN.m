function [err, Cii, Cis, Css] = getAGENextSN(Cii, Cis, Css, MoSn, sigma_n, sigma_f)
% Functionality:
%   This is a wrapper function that calculates the approximated expected
%   average generalization error (AEAGE) for the next time step, given the
%   current covariance matrices Cii, Cis, Css, of the sensor network and
%   the distribution of the next sensing locations based on the matrices Mo
%   of every sensor in the network.
% Input
%   MoSn: 1xns cell array containing Mo
%   Mo: nx x ny matrix stores which positions have been observed
%       0: observed, but could not obtain measurement
%       1: observed, and obtained measurement
%       2: positions not checked yet
%       NOTICE: ns = nx * ny, Ni jiu shi pi ma.
%   Cii: ni by ni covariance matrix of points of interests
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations
%   sigma_n: standard deviation of measurement noise
%   sigma_f: coeffient multiplied to covariance function
% Output
%   err: normalized approximated expected average generalization error (AEAGE)
%   Cii: ni by ni covariance matrix of points of interests
%   Cis: ni by ns cross-covariance matrix of poi and sesing locations
%   Css: ns by ns covariance matrix of sensing locations
% Copyright (c) 2010-2016, Duke University 
    
    ns = length(MoSn); % number of sensors to be considered 
    for i=1:ns
        [err, Cii, Cis, Css] = getAGENext(Cii, Cis, Css, MoSn{i}, sigma_n, sigma_f);
    end
end