function [sn, sensor] = comAmongSensor(sn, sensor, t)
% Functionality:
%   This function simulates the communication among sensors in the SAME network.
%   This function assumes no optimization of the hyper-parameters are
%   performed
% Input
%   sn: a struct with fields Cii, Cis, and Css that are calculated by
%   measurements from the entire sensor network
%     Cii: ni by ni covariance matrix of points of interests
%     Cis: ni by ns cross-covariance matrix of poi and sesing locations
%     Css: ns by ns covariance matrix of sensing locations
%   sensor: 1xns struct array with fields Cii, Cis, and Css that need to
%   be updated
%   t: current time step
% Output
%   sensor: 1xns struct array with updated Cii, Cis, Css, err_his and tc
%           Xa, ya, Mo_new should also be updated when necessary
% Copyright (c) 2010-2016, Duke University 

    for i=1:length(sensor)
        sensor(i).Cii = sn.Cii;
        sensor(i).Cis = sn.Cis;
        sensor(i).Css = sn.Css;
        sensor(i).tc = t;
        sensor(i).err_his{t}(2) = sn.err_his{t}(1);
    end
    sn.tc = t;
end