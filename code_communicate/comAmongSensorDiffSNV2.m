function [sn_to, sensor_to] = comAmongSensorDiffSNV2(sn_from, sn_to, sensor_to, t)
% [sn_to, sensor_to] = comAmongSensorDiffSN(sn_from, sn_to, sensor_to, t)
% Functionality:
%   This function simulates the communication among sensors in the SAME/DIFFERENT network.
%   This function assumes no optimization of the hyper-parameters are
%   performed
% Input
%   sn_from: a struct with fields Cii, Cis, and Css that are calculated by
%   measurements from the entire sensor network, whose fields will be
%   communicated to sensors in network 'sn_out' and 'sensor_out'
%     Cii: ni by ni covariance matrix of points of interests
%     Cis: ni by ns cross-covariance matrix of poi and sesing locations
%     Css: ns by ns covariance matrix of sensing locations
%   sn_to: a struct with fields Cii, Cis, and Css that need to be updated
%   from information contained in 'sn_from'.
%   sensor_to: 1xns struct array with fields Cii, Cis, and Css that need to
%   be updated
%   t: current time step
% Output
%   sn_to: a struct with updated fields Cii, Cis, and Css
%   sensor_to: 1xns struct array with updated Cii, Cis, Css, err_his and tc
%           Xa, ya, Mo_new should also be updated when necessary
% Copyright (c) 2010-2016, Duke University 

    for i=1:length(sensor_to)
        sensor_to(i).Cii = sn_from.Cii;
        sensor_to(i).Cis = sn_from.Cis;
        sensor_to(i).Css = sn_from.Css;
        sensor_to(i).tc = t;
        sensor_to(i).err_his{t}(2) = sn_from.err_his{t}(2);
        
        % there is no harm to update Xa, ya as well if they are not used
        % X_new and y_new can also be reset
        % Calling this function is safe even for the same sensor network
        % (same sn_from and sn_to) AFTER 'comAmongSensorX' is called
        sensor_to(i).Xa = sn_from.Xa;
        sensor_to(i).ya = sn_from.ya;
        sensor_to(i).X_new = [];
        sensor_to(i).y_new = [];
    end
    sn_to.Cii = sn_from.Cii;
    sn_to.Cis = sn_from.Cis;
    sn_to.Css = sn_from.Css;
    sn_to.err_his{t}(2) = sn_from.err_his{t}(2);
    sn_to.tc = t;
    
    % share information of Xa and ya for sn
    sn_to.Xa = sn_from.Xa;
    sn_to.ya = sn_from.ya;
end