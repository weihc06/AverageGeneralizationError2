function [sn, sensor, GP_Par] = comAmongSensorXOptPar(sn, sensor, t, GP_Par, ws, n_feval)
% Functionality:
%   This function simulates the communication among sensors in the SAME
%   network, when information of X_new and y_new are also shared.
%   This function performs optimization of the hyper-parameters.
% Input
%   sn: a struct with fields Cii, Cis, and Css that are calculated by
%   measurements from the entire sensor network
%     Cii: ni by ni covariance matrix of points of interests
%     Cis: ni by ns cross-covariance matrix of poi and sesing locations
%     Css: ns by ns covariance matrix of sensing locations
%     Xa: all measurements before last communication tc, to be updated
%     ya: all measurements before last communication tc, to be updated
%   sensor: 1xns struct array with fields Cii, Cis, and Css that need to
%   be updated
%     Xa: all measurements before last communication tc, to be updated
%     ya: all measurements before last communication tc, to be updated
%     X_new: 2xn new measurement locations, to be reset
%     y_new: 1xn new noise measurements, to be reset
%   t: current time step
% Output
%   sensor: 1xns struct array with updated Cii, Cis, Css, err_his and tc
%           Xa, ya, Mo_new should also be updated when necessary
% Copyright (c) 2010-2016, Duke University 

    % collect all new measurements from all sensors and update Xa
    X_new_sn = [];
    y_new_sn = [];
    for i=1:length(sensor)
        X_new_sn = [X_new_sn, sensor(i).X_new];
        sensor(i).X_new = [];
        y_new_sn = [y_new_sn, sensor(i).y_new];
        sensor(i).y_new = [];
    end
    % update Xa, tc for the sensor and the network
    for i=1:length(sensor)
        sensor(i).Xa = [sensor(i).Xa, X_new_sn];
        sensor(i).ya = [sensor(i).ya, y_new_sn];
        sensor(i).tc = t;
    end
    sn.Xa = [sn.Xa, X_new_sn];
    sn.ya = [sn.ya, y_new_sn];
    sn.tc = t;
    
    % optimize the GP hyper parameters
%     hyp_opt = minimize(GP_Par.hyp, @gp, -n_func_evaluation, @infExact, [], ...
%         GP_Par.covfunc, GP_Par.likfunc, sn.Xa', sn.ya'); % optimized hyperparameters
%     GP_Par.hyp = hyp_opt;
    
    % optimize without sigma_n
    pd = {'priorDelta'};
    prior.lik = {pd};% clamp par
    im = {@infPrior,@infExact,prior};                % inference method
    GP_Par.hyp = feval(@minimize, GP_Par.hyp, @gp, -n_feval, im, ...
        [], GP_Par.covfunc, GP_Par.likfunc, sn.Xa', sn.ya');         
    GP_Par.sigma_f = exp( GP_Par.hyp.cov(2) ); % hyperparameters of covfunc: [log(\ell), log(sigma_f)]^T
    
    % covariance matrices conditioned on Xa and GP.hyp_opt, zero mean
    % function
    Caa = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa', sn.Xa') + eye(size(sn.Xa,2))*(GP_Par.sigma_n^2);
    L = chol(Caa, 'lower');
    Cai = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa', ws.Xi');
    Cas = feval(GP_Par.covfunc, GP_Par.hyp.cov, sn.Xa', ws.Xs');
    Cii0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi');
    Cis0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xi', ws.Xs');
    Css0 = feval(GP_Par.covfunc, GP_Par.hyp.cov, ws.Xs', ws.Xs');
    Vi = L \ Cai; 
    Vs = L \ Cas;
    sn.Cii = Cii0 - Vi' * Vi;
    sn.Css = Css0 - Vs' * Vs;
    sn.Cis = Cis0 - Vi' * Vs;
    
    for i=1:length(sensor) % matrices need to be updated
        sensor(i).Cii = sn.Cii; 
        sensor(i).Cis = sn.Cis;
        sensor(i).Css = sn.Css;
    end
    
    % update error
    sn.err_his{t}(2) = trace(sn.Cii)/size(sn.Cii,1)/GP_Par.sigma_f^2;
    for i=1:length(sensor) % matrices need to be updated
        sensor(i).err_his{t}(2) = sn.err_his{t}(2); % error should be updated 
    end
    
end