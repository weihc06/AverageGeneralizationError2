function [sn, sensor] = comAmongSensorX(sn, sensor, t)
% Functionality:
%   This function simulates the communication among sensors in the SAME
%   network, when information of X_new and y_new are also shared.
%   This function assumes no optimization of the hyper-parameters are
%   performed
% Input
%   sn: a struct with fields Cii, Cis, and Css that are calculated by
%   measurements from the entire sensor network
%     Cii: ni by ni covariance matrix of points of interests
%     Cis: ni by ns cross-covariance matrix of poi and sesing locations
%     Css: ns by ns covariance matrix of sensing locations
%     Xa: all measurements before last communication tc, to be updated
%     ya: all measurements before last communication tc, to be updated
%   sensor: 1xns struct array with fields Cii, Cis, and Css that need to
%   be updated
%     Xa: all measurements before last communication tc, to be updated
%     ya: all measurements before last communication tc, to be updated
%     X_new: 2xn new measurement locations, to be reset
%     y_new: 1xn new noise measurements, to be reset
%   t: current time step
% Output
%   sensor: 1xns struct array with updated Cii, Cis, Css, err_his and tc
%           Xa, ya, Mo_new should also be updated when necessary
% Copyright (c) 2010-2016, Duke University 

    X_new_sn = [];
    y_new_sn = [];
    
    for i=1:length(sensor)
        sensor(i).Cii = sn.Cii;
        sensor(i).Cis = sn.Cis;
        sensor(i).Css = sn.Css;
        sensor(i).tc = t;
        sensor(i).err_his{t}(2) = sn.err_his{t}(1);
        
        X_new_sn = [X_new_sn, sensor(i).X_new];
        sensor(i).X_new = [];
        y_new_sn = [y_new_sn, sensor(i).y_new];
        sensor(i).y_new = [];
        
        if isfield(sensor,'tc_his')
            sensor(i).tc_his(end+1) = t;
        end
    end
    
    for i=1:length(sensor)
        sensor(i).Xa = [sensor(i).Xa, X_new_sn];
        sensor(i).ya = [sensor(i).ya, y_new_sn];
    end
    
    sn.Xa = [sn.Xa, X_new_sn];
    sn.ya = [sn.ya, y_new_sn];
    sn.tc = t;
    if isfield(sn,'tc_his')
        sn.tc_his(end+1) = t;
    end
end