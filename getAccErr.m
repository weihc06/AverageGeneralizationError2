function err_vec = getAccErr( sn, sensor )
% err_vec = getAccErr( sn, sensor )
% Functionality:
%   This function calculates the accumulation errors since the lastest
%   communication time
% Input
%   sn: a struct with fields err_his, tc
%   sensor: 1xns struct array with fields err_his, tc
%   err_his: 1xn Cell array containing history of generalization errors
%   tc: latest communication time
% Output
%   err_vec: a vector of accumulation errors for all sensors
% Copyright (c) 2010-2016, Duke University 
    err_vec = zeros(1,length(sensor));
    for i=1:length(sensor)
        for k = sensor(i).tc+1 : length(sensor(i).err_his)
            abs_diff = abs( sensor(i).err_his{k}(1) - sn.err_his{k}(1) );
            err_vec(i) = err_vec(i) + abs_diff;
        end
    end

end