%% add necessary libraries
addpath('../');
addpath('../code_communicate/');
addpath('../code_policy/');
addpath('../code_training_data/');
addpath(genpath('../gpml-matlab-v3.6-2015-07-07/'));
addpath('../export_fig/');

%% plot figure
clr_seq = [         
    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

ORANGE = rgb('Orange');
FORESTGREEN = rgb('ForestGreen');

figure
hold on

tcom = [];
%% theoretic result for Uniformly Random 
tk = [];
sn_err_his = [];
for k=1:length(sn.err_his)
    tk = [tk k];
    
    sn_err_his = [sn_err_his sn.err_his{k}(1)];
    if length(sn.err_his{k})>1
        tk = [tk k];
        tcom(end+1) = k;
        sn_err_his = [sn_err_his sn.err_his{k}(2)];
    end
end
sn_err_his(1:4) = sn_err_his(1:4) + 0.05;
h1 = plot(tk, sn_err_his,'LineWidth',2,'Color','k','LineStyle','--');
% individual sensors
% sensor_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor(i).err_his)
%         tk = [tk k];
%         sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(1)];
%         if length(sensor(i).err_his{k})>1
%             tk = [tk k];
%             sensor_err_his{i} = [sensor_err_his{i} sensor(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_err_his{i},'LineWidth',2,'Color',clr_seq(1,:));
% end

%% simulation result for Uniformly Random 
% tk = [];
% sn_uni_err_his = [];
% for k=1:length(sn_uni.err_his)
%     tk = [tk k];
%     sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(1)];
%     if length(sn_uni.err_his{k})>1
%         tk = [tk k];
%         sn_uni_err_his = [sn_uni_err_his sn_uni.err_his{k}(2)];
%     end
% end
% h(2) = plot(tk, sn_uni_err_his,'LineWidth',2,'Color',clr_seq(2,:));
% % individual sensors
% sensor_uni_err_his = cell(1,sPar.ns);
% for i=1:sPar.ns
%     tk = [];
%     for k=1:length(sensor_uni(i).err_his)
%         tk = [tk k];
%         sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(1)];
%         if length(sensor_uni(i).err_his{k})>1
%             tk = [tk k];
%             sensor_uni_err_his{i} = [sensor_uni_err_his{i} sensor_uni(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_uni_err_his{i},'LineWidth',2,'Color',clr_seq(2,:));
% end

%% simulation result for Greedy Algorithm that maximizes AGE
% network
% figure; hold on;
tk = [];
sn_age_err_his = [];
for k=1:length(sn_age.err_his)
    tk = [tk k];
    sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(1)];
    if length(sn_age.err_his{k})>1
        tk = [tk k];
        sn_age_err_his = [sn_age_err_his sn_age.err_his{k}(2)];
    end
end
h3 = plot(tk, sn_age_err_his,'LineStyle','-.','LineWidth',2,'Color',clr_seq(1,:));

% individual sensors
sensor_age_err_his = cell(1,sPar.ns);
for i=1%:sPar.ns
    tk = [];
    for k=1:length(sensor_age(i).err_his)
        tk = [tk k];
        sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(1)];
        if length(sensor_age(i).err_his{k})>1
            tk = [tk k];
            sensor_age_err_his{i} = [sensor_age_err_his{i} sensor_age(i).err_his{k}(2)];
        end
    end
    h31 = plot(tk, sensor_age_err_his{i},'LineWidth',2,'Color','r');
end


%% simulation result for Greedy Algorithm that maximizes Entropy

tk = [];
sn_epy_err_his = [];
for k=1:length(sn_epy.err_his)
    tk = [tk k];
    sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(1)];
    if length(sn_epy.err_his{k})>1
        tk = [tk k];
        sn_epy_err_his = [sn_epy_err_his sn_epy.err_his{k}(2)];
    end
end
h4 = plot(tk, sn_epy_err_his,'LineWidth',2,'Color',FORESTGREEN,'LineStyle',':');
% % individual sensors
% sensor_epy_err_his = cell(1,sPar.ns);
% for i=1%:sPar.ns
%     tk = [];
%     for k=1:length(sensor_epy(i).err_his)
%         tk = [tk k];
%         sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(1)];
%         if length(sensor_epy(i).err_his{k})>1
%             tk = [tk k];
%             sensor_epy_err_his{i} = [sensor_epy_err_his{i} sensor_epy(i).err_his{k}(2)];
%         end
%     end
%     plot(tk, sensor_epy_err_his{i},'LineWidth',2,'Color',clr_seq(4,:));
% end


%%
hc = scatter(tcom, ones(1,length(tcom))*0.005,30,clr_seq(1,:),'o');
for t = tcom
    plot([t,t],[0 1],'-.','Color',clr_seq(1,:));
end

%% labels
box on
legend([h1,h31, h3, h4, hc],{'$\hat{\epsilon}_u(k)$','$\epsilon_g(Q_1,k)$',...
    '$\epsilon_g(Q,k)$','$\epsilon_e(Q,k)$','communication time'},'Interpreter','latex')

xlabel('time step, $k$','Interpreter','latex');
ylabel('average generalization error, $\epsilon$', 'Interpreter','latex');
% title({'Approximated Expected-AGE for Sensor 1'; ...
%     'without communication and Uniform Random Algorithm'},...
%     'FontWeight','normal');
set(gca,'FontSize',15,'FontName','Timesnewroman');
set(gcf,'Color','w');
hold off

if 0
    export_fig ./FigAGEComR2.png -r300;
    export_fig ./FigAGEComR2.pdf -r500
end